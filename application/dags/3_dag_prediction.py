from airflow import DAG
from airflow.operators.python import PythonOperator
from datetime import datetime
from airflow.operators.mysql_operator import MySqlOperator
from scripts import Shaping_data_script_test, NN_application_scripts

dag = DAG(dag_id="3_dag_prediction",
          start_date=datetime(2021, 1, 1),
          schedule_interval="@monthly",
          catchup=False)

extract_vehc_watched = MySqlOperator(dag=dag,
                                     mysql_conn_id='mysql',
                                     task_id='extract_predicted_vehicles',
                                     sql='sql_scripts/recall_vehicles.sql')

prediction = PythonOperator(task_id='find_vehicles_to_keep',
                            python_callable=NN_application_scripts.main,
                            dag=dag
                            )
python_shaping_test = PythonOperator(
    task_id='shaping_test',
    python_callable=Shaping_data_script_test.main,
    dag=dag
)

python_shaping_test >> prediction >> extract_vehc_watched
