#! /usr/bin/env python3


'''
Ce script va compiler et fitter le modèle de réseaux de neurones que nous avons construit.
Il prend en entrée les bases d'entrainement et renvoie en sortie le modèle calculé.
'''


def main():
    from keras.layers import Dense
    from keras.layers import Dropout
    from keras.models import Sequential
    from tensorflow.keras.optimizers import Adam
    from keras.regularizers import l1_l2
    import pickle

    X_input_filename = '/tmp/X_full_train.pkl'
    y_input_filename = '/tmp/y_train_encoded_full.pkl'
    model_output_filename = '/tmp/nn_to_pred.pkl'

    with open(X_input_filename, 'rb') as f2:
        X_train_encoded = pickle.load(f2)
    with open(y_input_filename, 'rb') as f3:
        y_train_encoded = pickle.load(f3)
    '''with open('/home/amouchet/Desktop/vehicle_analysis/application/' + c, 'rb') as f2:
        X_test_encoded = pickle.load(f2)
    print('train data loaded')

    cols = X_test_encoded.columns.intersection(X_train_encoded.columns).tolist()
    cols = list(cols)

    with open('/tmp/cols.pkl','wb') as fh:
        pickle.dump(cols, fh)

    del X_test_encoded'''
    shape = X_train_encoded.shape[1]

    print(X_train_encoded.head())

    model = Sequential()
    layer = Dense(200, input_shape=(shape,), activation='relu', kernel_regularizer=l1_l2(l1=0.001, l2=0.001))
    model.add(layer)
    # model.add(Dropout(0.1))
    model.add(Dense(100, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(50, activation='relu'))
    model.add(Dropout(0.5))
    # model.add(Dense(20,activation='relu'))
    # model.add(Dense(1,activation='sigmoid'))
    model.add(Dense(5, activation='softmax'))
    opt = Adam(learning_rate=0.0001)
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['FalsePositives', 'TruePositives', 'AUC',
                                                                           'CategoricalAccuracy', 'Precision'])
    print('model compiled')
    history = model.fit(X_train_encoded, y_train_encoded, epochs=5, batch_size=1000)
    print('model fitted')
    del X_train_encoded
    del y_train_encoded

    print('train data deleted')

    with open(model_output_filename, 'wb') as fh:
        pickle.dump(model, fh)


if __name__ == "__main__":
    main()
