USE HISTOVEC;

DROP TABLE IF EXISTS table_train;
DROP TABLE IF EXISTS table_test;

CREATE TABLE table_test (dos_id BIGINT NOT NULL, CTEC_PUISS_CV VARCHAR(64) NULL, dept INT NULL,
marque VARCHAR(64) NULL, nom_commercial VARCHAR(128) NULL, date_premiere_immat VARCHAR(64) NULL,
date_emission_CI VARCHAR(64) NULL, nb_titulaires INT NULL, is_apte_a_circuler INT NULL, pers_locataire INT NULL,
code_energie INT NULL, nb_sinistres INT NULL, critair INT NULL, CTEC_RLIB_CARROSSERIE_NAT VARCHAR(64) NULL,
utac_encrypted_vin VARCHAR(64) NOT NULL, utac_encrypted_immat VARCHAR(64) NOT NULL);

LOAD DATA INFILE '/var/lib/mysql/table_import.csv' into table table_test fields terminated by ',' ignore 1 LINES;

CREATE TABLE source_logs (datation VARCHAR(64) NULL, utac_encrypted_vin VARCHAR(64) NOT NULL,
utac_encrypted_immat VARCHAR(64) NOT NULL);

LOAD DATA INFILE '/var/lib/mysql/source_logs_sql.csv' into table table_test fields terminated by ',' ignore 1 LINES;

UPDATE table_test
SET date_premiere_immat = LEFT(date_premiere_immat,10 );

UPDATE table_test
SET date_emission_CI = LEFT(date_emission_CI,10 );

ALTER TABLE `table_test`
CHANGE `date_premiere_immat` `date_premiere_immat` date NULL AFTER `nom_commercial`;

ALTER TABLE `table_test`
CHANGE `date_emission_CI` `date_emission_CI` date NULL AFTER `date_premiere_immat`;

ALTER TABLE table_test
ADD COLUMN age_vehicule int, ADD age_certificat int, ADD unique_id VARCHAR(64);

UPDATE table_test
SET unique_id = CONCAT(utac_encrypted_vin,'_',utac_encrypted_immat);

UPDATE table_test
SET age_vehicule = DATEDIFF(CURDATE(), date_premiere_immat),
 age_certificat = DATEDIFF(CURDATE(),date_emission_CI);


CREATE TABLE table_train LIKE table_test;
INSERT INTO table_train SELECT * FROM table_test;

ALTER TABLE table_test
ADD COLUMN datation varchar (64);
ALTER TABLE table_train
ADD COLUMN datation varchar (64);

UPDATE source_logs
SET datation = LEFT(datation, 8);

ALTER TABLE source_logs
ADD COLUMN unique_id VARCHAR(64);

UPDATE source_logs
SET unique_id = CONCAT(utac_encrypted_vin,'_',utac_encrypted_immat);

CREATE TABLE logs_train LIKE source_logs;
INSERT INTO logs_train SELECT * FROM source_logs  WHERE datation < '2022-03-01';

create table counting (unique_id varchar(128), counts int);
create table counting_train (unique_id varchar(128), counts int);

Insert counting
SELECT unique_id, COUNT(unique_id) AS count
FROM source_logs
GROUP BY unique_id;

Insert counting_train
SELECT unique_id, COUNT(unique_id) AS count
FROM logs_train
GROUP BY unique_id;

ALTER TABLE table_test
ADD COLUMN nb_observations int;
ALTER TABLE table_train
ADD COLUMN nb_observations int;

UPDATE table_test as ti
JOIN counting
on ti.unique_id = counting.unique_id
SET ti.nb_observations = counting.counts;

UPDATE table_train ti
JOIN counting_train
on ti.unique_id = counting_train.unique_id
SET ti.nb_observations = counting_train.counts;

UPDATE table_test
SET table_test.datation = (SELECT MAX(datation)
               FROM source_logs
               WHERE source_logs.unique_id = table_test.unique_id);

UPDATE table_train
SET table_train.datation = (SELECT MAX(datation)
               FROM logs_train
               WHERE logs_train.unique_id = table_train.unique_id);

ALTER TABLE table_test
ADD COLUMN is_observed int(1);
ALTER TABLE table_train
ADD COLUMN is_observed int(1);

alter table table_test change is_observed INT zerofill not null;
alter table table_train change is_observed INT zerofill not null;

UPDATE table_test
SET is_observed = CASE
   WHEN MONTH(datation)=3 THEN 1
   ELSE 0
END;


UPDATE table_train
SET is_observed = CASE
   WHEN nb_observations > 0 THEN 1
   ELSE 0
END;


SELECT *
FROM table_train
INTO OUTFIlE '/var/lib/mysql-files/vehicule_data_train_grouped.csv';

SELECT *
FROM table_test
INTO OUTFIlE '/var/lib/mysql-files/vehicule_data_test_grouped.csv';

