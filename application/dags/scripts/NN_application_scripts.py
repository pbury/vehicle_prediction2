#! /usr/bin/env python3



'''
Ce script permet de tester le modèle de réseaux de neurone fitté durant le script précédent.
Le script prend en entrée le modèle calculé précédemment,la base de test contenant les variables et la base de test
 concernant la variable objectif (observation véhicule).
 On récupère en sortie l'image de la matrice de confusion issue des prédictions.
'''


def main():
    import pickle
    import matplotlib.pyplot as plt
    import pandas as pd
    import seaborn as sns
    from sklearn.metrics import multilabel_confusion_matrix
    import mysql.connector as mysql
    from sqlalchemy import create_engine
    import os

    db_connection = create_engine(
        "mysql+pymysql://{user}:{pw}@{host}/{db}".format(host=os.getenv("MYSQL_HOSTNAME"), db=os.getenv("MYSQL_DBNAME"),
                                                         user=os.getenv("MYSQL_USER"),
                                                         pw=os.getenv("MYSQL_ROOT_PASSWORD")))

    X_input_filename = 'X_full_test.pkl'
    y_input_filename = 'y_test_encoded_full.pkl'
    model_input_filename = 'nn_to_pred.pkl'
    testing_output_filename = 'confusion_matrix.png'

    with open(X_input_filename, 'rb') as f2:
        X_test_encoded = pickle.load(f2)
    with open(y_input_filename, 'rb') as f3:
        y_test_encoded = pickle.load(f3)
    with open(model_input_filename, 'rb') as fh:
        model = pickle.load(fh)

    print('test data loaded')
    y_predicted = model.predict(X_test_encoded) > 0.5
    del X_test_encoded
    print('predictions calculated')
    df_preds = pd.DataFrame(y_predicted, index=y_test_encoded.index)
    print('predictions calculated')
    df_preds['ref_vehc_id'] = df_preds.index
    df_preds.reset_index(inplace=True)

    df_preds.to_sql(con=db_connection, name='predicted_vehicles', if_exists='replace', index=False)
    del df_preds


    def print_confusion_matrix(confusion_matrix, axes, class_label, class_names, fontsize=14):

        df_cm = pd.DataFrame(
            confusion_matrix, index=class_names, columns=class_names,
        )

        try:
            heatmap = sns.heatmap(df_cm, annot=True, fmt="d", cbar=False, ax=axes)
        except ValueError:
            raise ValueError("Confusion matrix values must be integers.")
        heatmap.yaxis.set_ticklabels(heatmap.yaxis.get_ticklabels(), rotation=0, ha='right', fontsize=fontsize)
        heatmap.xaxis.set_ticklabels(heatmap.xaxis.get_ticklabels(), rotation=45, ha='right', fontsize=fontsize)
        axes.set_ylabel('True label')
        axes.set_xlabel('Predicted label')
        axes.set_title("Confusion Matrix for the class - " + class_label)

    def disp_conf_matrix(y_predicted_var, y_test_encoded_var, labels_var):
        mat = multilabel_confusion_matrix(y_test_encoded_var, y_predicted_var)
        fig_var, ax = plt.subplots(3, 2, figsize=(12, 7))
        for axes, cfs_matrix, label in zip(ax.flatten(), mat, labels_var):
            print_confusion_matrix(cfs_matrix, axes, label, ["Unobserved", "Observed"])
        return fig_var

    labels = y_test_encoded.columns
    fig = disp_conf_matrix(y_predicted, y_test_encoded, labels)
    fig.tight_layout()
    fig.savefig(testing_output_filename)

if __name__ == "__main__":
    main()
