#! /usr/bin/env python3
'''
Ce script permet de déterminer la densité de véhicules par habitant dans les départements de France.
Cette variable sera ensuite introduite dans le modèle et permet de prendre en compte les spécificités régionales.
On entre la base d'entrainement dont on ne gardera que la colonne département en variable d'entrée et en sortie nous 
récupérons un dataframe dont l'une des colonnes est le département et l'autre le nombre de voitures par habitant.
'''
# Need to install html5lib if not already done
import os


def main():
    import requests  # library to handle requests
    from bs4 import BeautifulSoup  # library to parse HTML documents
    import pandas as pd
    import re
    import mysql.connector as mysql
    from mysql.connector import Error
    from sqlalchemy import create_engine

    mysql_hostname = os.getenv("MYSQL_HOSTNAME") # "7ed08753246f_mysql_v3b"
    mysql_dbname = os.getenv("MYSQL_DBNAME") # "histovec"
    mysql_uname = os.getenv("MYSQL_USER") #"root"
    mysql_pwd = os.getenv("MYSQL_ROOT_PASSWORD") #"example"

    db_connection = create_engine("mysql+pymysql://{user}:{pw}@{host}/{db}".format(host=mysql_hostname, db=mysql_dbname,
                                                                                   user=mysql_uname, pw=mysql_pwd))

    #db_connection = mysql.connect(host='mysql_v3b', database='HISTOVEC', user='root', password='example')

    # get the response in the form of html
    wikiurl = "https://fr.wikipedia.org/wiki/Liste_des_d%C3%A9partements_fran%C3%A7ais_class%C3%A9s_" \
              "par_population_et_superficie"
    table_class = "wikitable sortable jquery-tablesorter"
    response = requests.get(wikiurl)
    #print(response.status_code)

    # parse data from the html into a beautifulsoup object
    soup = BeautifulSoup(response.text, 'html.parser')
    indiatable = soup.find('table', {'class': "wikitable"})

    df_dept = pd.read_html(str(indiatable), flavor='html5lib')
    # convert list to dataframe
    df_dept = pd.DataFrame(df_dept[0])

    df_dept.columns = df_dept.columns.droplevel()
    n, m = df_dept.shape
    for i in range(n):
        for j in range(m):
            df_dept.iloc[i, j] = re.sub("[\(\[].*?[\)\]]", "", str(df_dept.iloc[i, j]))

    cols = list(df_dept.columns)
    n = len(cols)
    for i in range(n):
        cols[i] = re.sub("[\(\[].*?[\)\]]", "", cols[i])
    df_dept.columns = cols

    # On ne garde que la dernière mesure de population de 2019
    df_dept = df_dept[['CodeInsee', '2019']]
    df_dept['2019'] = df_dept['2019'].str.replace("\xa0", "").replace('256518[2](2017)', '256518').astype(int)

    # On crée une seule ligne pour les deux départements Corses et une seule ligene pour les départements d'Outre-mer.
    l_dept = df_dept[(df_dept['CodeInsee'] == '2A') | (df_dept['CodeInsee'] == '2B')].sum()
    OM = df_dept[(df_dept['CodeInsee'] == '971') | (df_dept['CodeInsee'] == '972') | (df_dept['CodeInsee'] == '973') |
                 (df_dept['CodeInsee'] == '974') | (df_dept['CodeInsee'] == '975') |
                 (df_dept['CodeInsee'] == '976')].sum()
    df_dept = df_dept.append(OM, ignore_index=True)
    df_dept = df_dept.append(l_dept, ignore_index=True)
    df_dept.loc[df_dept['CodeInsee'] == '2B2A', 'CodeInsee'] = 20
    df_dept.loc[df_dept['CodeInsee'] == '974971972973976', 'CodeInsee'] = 97
    # De la base globale on extrait la colonne département


    df_whole = pd.read_sql('SELECT * FROM dept_amount', con=db_connection)

    df_whole['dept'] = df_whole['dept'].astype(int)
    df_dept = df_dept[(df_dept['CodeInsee'] != '6AE') & (df_dept['CodeInsee'] != '69M') & (df_dept['CodeInsee'] != '69D')
                      & (df_dept['CodeInsee'] != '2A') & (df_dept['CodeInsee'] != '2B')]

    df_dept.loc[:, 'CodeInsee'] = df_dept.loc[:, 'CodeInsee'].astype(int)
    df_departement = pd.merge(df_dept, df_whole, left_on='CodeInsee', right_on='dept')
    df_departement['vehic_per_hab'] = df_departement['nb']/df_departement['2019']

    del df_whole
    del df_dept

    df_departement.drop(['CodeInsee', '2019', 'nb'], inplace=True, axis=1)
    df_departement.to_sql(name='vehicule_per_hab', con=db_connection, if_exists='replace', index=False)

    del df_departement
'''
    try:
        if db_connection.is_connected():
            cursor = db_connection.cursor()
            cursor.execute("select database();")
            record = cursor.fetchone()
            print("You're connected to database: ", record)
            cursor.execute('DROP TABLE IF EXISTS vehicule_per_hab;')
            print('Creating table....')
            # in the below line please pass the create table statement which you want #to create
            cursor.execute(
                "CREATE TABLE vehicule_per_hab(dept INT NULL, vehic_per_hab FLOAT NULL)")
            print("Table is created....")
            # loop through the data frame
            for i, row in df_departement.iterrows():
                # here %S means string values 
                sql = "INSERT INTO HISTOVEC.vehicule_per_hab VALUES ( %s, %s)"
                cursor.execute(sql, tuple(row))
                # the connection is not auto committed by default, so we must commit to save our changes
                db_connection.commit()
    except Error as e:
        print("Error while connecting to MySQL", e)'''

if __name__ == "__main__":
    main()
