import pandas as pd
import re

df_to_clean = pd.read_excel('denom_commerciale_bis.xlsx', dtype={'Equiv':str, 'nom_commercial':str, 'nb':int})
df_to_clean['Equiv'].value_counts().head(14).sum()/df_to_clean['Equiv'].value_counts().sum()
def detec_number(name,n_carac=3):
    name=str(name)
    l=['0','1','2','3','4','5','6','7','8','9']
    if name[0] in l:
        S=""
        i=name[0]
        c=0
        while i in l and (c+1)<len(name):
            S+=i
            c+=1
            i=name[c]

    else:
        S=""
    if len(S)>=n_carac:
        return(S)
    else:
        return(name)

def detec_name(name,L_modeles):
    name=str(name)
    for i in L_modeles :
        motif = re.compile(i)
        obj = motif.search(name)
        if obj:
            name=i
            break
    return(name)

def FIAT(df_to_clean):

    df_fiat=df_to_clean[df_to_clean['Equiv']=='FIAT'].copy()
    df_fiat['new_nom_commercial']=df_fiat['nom_commercial']
    df_fiat.head()
    df_fiat['new_nom_commercial']= df_fiat['new_nom_commercial'].apply(detec_number)
    df_fiat['new_nom_commercial']= df_fiat['new_nom_commercial'].apply(detec_number,args=(4,))
    df_fiat.loc[df_fiat['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_fiat[df_fiat['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_fiat[df_fiat['nb'].astype(str).str.isnumeric()==False]['nb']
    df_fiat.loc[df_fiat['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_fiat = df_fiat.groupby(['Equiv','nom_commercial','new_nom_commercial']).agg({'nb':'sum'}).reset_index()

    L_modeles=['4x4', 'PUNTO', 'ABARTH','PANDA','DUCATO','DOBLO','SCUDO','TIPO','FIORINO','STILO','BRAVO','TALENTO','MULTIPLA','SEICENTO',
           'BRAVA','ULYSSE','IDEA','PALIO','MAREA','FREEMONT','CINQUECE','CROMA','BARCHETTA','UNO','FULLBACK','WELCOME','STRADA',
           'PALIOWEEKE','GENESIS','COUPE','CHALLENGER','TEMPRA','DYNAMIC','FENDT','F72','F74','F63','F69','EMOTION','500','F73','F83',
           'BEL HORIZON','I740','P730','G740','DUC','P746','P670','P716','AUROS','FOURGON','DYN','ARTO','T746','G741','I074','G730','G690',
          'G742','ARTO','AUTOCAMP','VAN','FOURG','FRANCIA','FLASH']

    df_fiat['new_nom_commercial']= df_fiat['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_fiat = df_fiat.groupby(['Equiv','nom_commercial','new_nom_commercial']).agg({'nb':'sum'}).reset_index()

    List_final_name=df_fiat[(df_fiat['nb']>2000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_fiat.loc[df_fiat['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_fiat)

def RENAULT(df_to_clean):
    df_renault=df_to_clean[df_to_clean['Equiv']=='RENAULT'].copy()
    L_modeles=['CLIO','MEGANE','TWINGO','MEGANE','KANGOO','CAPTUR','TRAFIC','MASTER','LAGUNA','ESPACE','MODUS','KADJAR','SCENIC','ZOE','EXPRESS',
          'KOLEOS','TALISMAN','R5','SAFRANE','R19','R4','R19','R21','CHALLENGER','MAGNUM','R25','MAXITY','TWIZY','THALIA','CLI','R8','R11',
          'ALPINE','M{GANE','DY','ESTAFETTE','TWI','GRAPHITE','CL','R12','R20','R18','R16','5 ','3 ','21 ','R6','4L','KAN','RODEO','ANTHEA',
          'R9','SUPER 5','R7','4CV','R10','SAF','LAG']

    df_renault['new_nom_commercial'] = df_renault['nom_commercial']
    df_renault['new_nom_commercial'] = df_renault['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_renault = df_renault.groupby(['Equiv', 'nom_commercial', 'new_nom_commercial']).agg({'nb': 'sum'}).reset_index()
    df_renault[df_renault['nb'] > 100].sort_values('nb', ascending=False).head(50)
    df_renault[df_renault['nb'] > 10000]['nb'].sum()/df_renault['nb'].sum()
    List_final_name=df_renault[(df_renault['nb'] > 10000)].sort_values('nb', ascending=False)['new_nom_commercial'].to_list()
    df_renault.loc[df_renault['new_nom_commercial'].isin(List_final_name) == False, 'new_nom_commercial'] = 'DIVERS'
    return(df_renault)

def PEUGEOT(df_to_clean):
    df_peugeot=df_to_clean[df_to_clean['Equiv'] == 'PEUGEOT'].copy()
    df_peugeot['new_nom_commercial'] = df_peugeot['nom_commercial']
    df_peugeot.loc[df_peugeot['nb'].astype(str).str.isnumeric() == False, 'new_nom_commercial'] = df_peugeot[df_peugeot['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_peugeot[df_peugeot['nb'].astype(str).str.isnumeric() == False]['nb']
    df_peugeot.loc[df_peugeot['nb'].astype(str).str.isnumeric() == False, 'nb'] = 1
    df_peugeot['new_nom_commercial'] = df_peugeot['new_nom_commercial'].apply(detec_number)
    L_modeles = ['PARTNER','EXPERT','BOXER','LUDIX','KISBEE','BIPPER','VIVACITY','RIFTER','SATELIS','VOGUE','TREKKER','FRG','PULSION','GEOPOLIS',
          'ZENITH','BOXC','FOX','VOG','XPS','BOX','CYCL','BX','EXP','PART','TKR','VIVA','ELYSTAR','SPEED','JET','J7','J9','ITS','J5','FIGHT',
          'LOOXOR','MBK','SOLEX','XP6','BUXY','GRIFFON','ELYSEO','XR6','FT 270C','FT 310C','B9','BIP','CLIC']

    df_peugeot['new_nom_commercial'] = df_peugeot['new_nom_commercial'].apply(detec_name, args=(L_modeles,))
    df_peugeot = df_peugeot.groupby(['Equiv', 'nom_commercial', 'new_nom_commercial']).agg({'nb': 'sum'}).reset_index()
    df_peugeot[(df_peugeot['nb'] > 10000)]['nb'].sum()/df_peugeot['nb'].sum()
    List_final_name=df_peugeot[(df_peugeot['nb'] > 10000)].sort_values('nb', ascending=False)['new_nom_commercial'].to_list()
    df_peugeot.loc[df_peugeot['new_nom_commercial'].isin(List_final_name) == False, 'new_nom_commercial'] = 'DIVERS'
    return(df_peugeot)

def CITROEN(df_to_clean):
    df_citroen = df_to_clean[df_to_clean['Equiv'] == 'CITROEN'].copy()
    df_citroen['new_nom_commercial'] = df_citroen['nom_commercial']
    df_citroen.loc[df_citroen['nb'].astype(str).str.isnumeric() == False, 'new_nom_commercial'] = \
        df_citroen[df_citroen['nb'].astype(str).str.isnumeric() == False]['new_nom_commercial'].astype(str)+\
        df_citroen[df_citroen['nb'].astype(str).str.isnumeric() == False]['nb']
    df_citroen.loc[df_citroen['nb'].astype(str).str.isnumeric() == False, 'nb'] = 1
    L_modeles=['PICASSO', 'BERLINGO', 'JUMPY', 'AIRCROSS', 'SAXO', 'DS3', 'CACTUS', 'DS5', 'EVASION', 'NEMO', 'XSARA',
           'EVASION', 'MEHARI', 'XANTIA', 'JUMPER', 'C3', 'C5', 'ZX', 'DS4', 'DS5', 'DS21', 'C4', 'C2','FLASH', 'C15',
           'C6', 'C8', 'C1', 'AX', 'XM', 'TORPEDO', 'BX', '2CV', 'CX', 'VISA', 'JUMPR', 'C-CROSSER', 'C-ELYSEE',
           'MODULIS', 'GENESIS', 'BERLIGO', 'XANT']

    df_citroen['new_nom_commercial'] = df_citroen['new_nom_commercial'].apply(detec_name, args=(L_modeles,))
    df_citroen = df_citroen.groupby(['Equiv', 'nom_commercial', 'new_nom_commercial']).agg({'nb': 'sum'}).reset_index()
    df_citroen[df_citroen['nb'] > 0].sort_values('nb', ascending=False)
    df_citroen[df_citroen['nb'] > 10000]['nb'].sum()/df_citroen['nb'].sum()
    List_final_name = df_citroen[(df_citroen['nb'] > 10000)].sort_values('nb', ascending=False)['new_nom_commercial'].to_list()
    df_citroen.loc[df_citroen['new_nom_commercial'].isin(List_final_name) == False, 'new_nom_commercial'] = 'DIVERS'
    return(df_citroen)

def MERCEDES(df_to_clean):
    df_mercedes = df_to_clean[df_to_clean['Equiv'] == 'MERCEDES'].copy()
    df_mercedes['new_nom_commercial'] = df_mercedes['nom_commercial']
    df_mercedes.loc[df_mercedes['nb'].astype(str).str.isnumeric() == False, 'new_nom_commercial'] = \
        df_mercedes[df_mercedes['nb'].astype(str).str.isnumeric() == False]['new_nom_commercial'].astype(str)+\
        df_mercedes[df_mercedes['nb'].astype(str).str.isnumeric() == False]['nb']
    df_mercedes.loc[df_mercedes['nb'].astype(str).str.isnumeric() == False, 'nb'] = 1
    L_modeles=['CLASSE A', 'CLASSE C', 'VITO', 'SPRINTER', 'CLASSEA', 'CLASSEML', 'GLC', 'GLB', 'GLE', 'GLA', 'CLA',
               'AROCS', 'INTEGRO', 'AMG GT', 'TOURISMO', '318', '109', 'C220', 'ACTROS', 'ATEGO', 'ANTOS', 'AMG C63',
               'A308', 'A312', 'C280', '111 CDI', '115 CDI', '216 CDI', '120 CDI', 'SERIEC', '300GD', '220', '230', '240',
               '280', '320', '200']
    df_mercedes['new_nom_commercial'] = df_mercedes['new_nom_commercial'].apply(detec_name, args=(L_modeles,))
    df_mercedes = df_mercedes.groupby(['Equiv', 'nom_commercial', 'new_nom_commercial']).agg({'nb': 'sum'}).reset_index()
    df_mercedes.sort_values('nb', ascending=False)
    df_mercedes[df_mercedes['nb'] > 5000]['nb'].sum()/df_mercedes['nb'].sum()
    List_final_name = df_mercedes[(df_mercedes['nb'] > 5000)].sort_values('nb', ascending=False)['new_nom_commercial'].to_list()
    df_mercedes.loc[df_mercedes['new_nom_commercial'].isin(List_final_name) == False, 'new_nom_commercial'] = 'DIVERS'
    return df_mercedes

def VOLKSWAGEN(df_to_clean):
    df_volkswagen = df_to_clean[df_to_clean['Equiv'] == 'VOLKSWAGEN'].copy()
    df_volkswagen['new_nom_commercial'] = df_volkswagen['nom_commercial']
    df_volkswagen['nb'] = df_volkswagen['nb'].astype(int)
    df_volkswagen.loc[df_volkswagen['nb'].astype(str).str.isnumeric() == False, 'new_nom_commercial'] = \
        df_volkswagen[df_volkswagen['nb'].astype(str).str.isnumeric() == False]['new_nom_commercial'].astype(str)+\
        df_volkswagen[df_volkswagen['nb'].astype(str).str.isnumeric() == False]['nb']
    df_volkswagen.loc[df_volkswagen['nb'].astype(str).str.isnumeric() == False, 'nb'] = 1
    L_modeles=['GOLF', 'POLO', 'TIGUAN', 'TOURAN', 'PASSAT', 'CADDY', 'CRAFTER', 'T5', 'VENTO', 'TRANSPORTER', 'TOUAREG',
               'CORRADO', 'SHARAN', 'SCORBEL', 'LT 35', 'ID.3', 'ID.4', 'SCIROCCO', 'FOX 1.2', 'TRANSPORT', 'LUPO', 'JETTA',
               'COMBI', 'LT 28', 'LT 32', 'EOS', 'FOX', 'BEETLE', 'COCCINELLE', 'CARAVELLE', 'T4', 'LT40', 'BORA', 'LT31',
               'LT35', 'MULTIVAN', 'LT 46', 'LT28', 'LT32', 'PHAETON', 'CALIFORNIA', 'BUGGY', 'SPEEDSTER', 'KARMANN',
               'ESCARABAJO', 'DERBY', 'PAS BREAK', 'BUS']

    df_volkswagen['new_nom_commercial'] = df_volkswagen['new_nom_commercial'].apply(detec_name, args=(L_modeles,))
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'COCCINELL', 'new_nom_commercial'] = 'COCCINELLE'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'COCINELLE', 'new_nom_commercial'] = 'COCCINELLE'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'COCCINNELLE', 'new_nom_commercial'] = 'COCCINELLE'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'JETTA', 'new_nom_commercial'] = 'BORA'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'LT 32', 'new_nom_commercial'] = 'LT32'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'LT 28', 'new_nom_commercial'] = 'LT28'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'NEW BEETL', 'new_nom_commercial'] = 'BEETLE'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'KOMBI', 'new_nom_commercial'] = 'COMBI'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'LT 46', 'new_nom_commercial'] = 'LT46'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'GOLG', 'new_nom_commercial'] = 'GOLF'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'GOLG VI', 'new_nom_commercial'] = 'GOLF'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'GOL', 'new_nom_commercial'] = 'GOLF'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'CALIFORNI', 'new_nom_commercial'] = 'CALIFORNIA'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'KARMAN GHIA', 'new_nom_commercial'] = 'KARMANN'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'PICK UP', 'new_nom_commercial'] = 'PICK-UP'
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'] == 'BUS', 'new_nom_commercial'] = 'MINIBUS'
    df_volkswagen[df_volkswagen['nb'] > 30].sort_values('nb', ascending=False)
    df_volkswagen[(df_volkswagen['nb'] > 10000)]['nb'].sum()/df_volkswagen['nb'].sum()
    List_final_name = df_volkswagen[(df_volkswagen['nb'] > 10000)].sort_values('nb',  ascending=False)['new_nom_commercial'].to_list()
    df_volkswagen.loc[df_volkswagen['new_nom_commercial'].isin(List_final_name) == False, 'new_nom_commercial'] = 'DIVERS'
    return df_volkswagen

def NISSAN(df_to_clean):
    df_nissan = df_to_clean[df_to_clean['Equiv'] == 'NISSAN'].copy()
    df_nissan['new_nom_commercial'] = df_nissan['nom_commercial']
    df_nissan.loc[df_nissan['nb'].astype(str).str.isnumeric() == False, 'new_nom_commercial'] = \
        df_nissan[df_nissan['nb'].astype(str).str.isnumeric() == False]['new_nom_commercial'].astype(str)+\
        df_nissan[df_nissan['nb'].astype(str).str.isnumeric() == False]['nb']
    df_nissan.loc[df_nissan['nb'].astype(str).str.isnumeric() == False, 'nb'] = 1
    df_nissan.loc[df_nissan['new_nom_commercial'] == 'PATHF', 'new_nom_commercial'] = 'PATHFINDER'
    df_nissan.loc[df_nissan['new_nom_commercial'] == 'MIC', 'new_nom_commercial'] = 'MICRA'
    df_nissan.loc[df_nissan['new_nom_commercial'] == 'PRIMA', 'new_nom_commercial'] = 'PRIMASTAR'
    L_modeles = ['LEAF', 'QASHQAI', 'JUKE', 'NAVARA', 'MICRA', 'NV300', 'PATROL', 'NOTE', 'NV200', 'NV400', 'PRIMERA',
                 'TERRANO', 'VANETTE', 'ALMERA', 'SUNNY', 'NT400', 'CABSTAR', 'X-TRAIL', 'NT500', 'PICK-UP', 'MAXIMA',
                 'PRIMASTAR', 'X-T 2,2DCI', 'B11', 'CAB', 'MULTITEL', 'ATLEON', 'PIXO', 'BLUEBIRD', 'PATHF', 'INTERSTAR',
                 'MURANO', 'PICK UP', 'TINO', 'MIC', 'SKYLINE', 'FOURGON', 'INFINIT', 'KUBISTAR', 'KUBI']
    df_nissan['new_nom_commercial'] = df_nissan['new_nom_commercial'].apply(detec_name, args=(L_modeles,))
    df_nissan = df_nissan.groupby(['Equiv', 'nom_commercial', 'new_nom_commercial']).agg({'nb': 'sum'}).reset_index()
    df_nissan.sort_values('nb', ascending=False)
    df_nissan[(df_nissan['nb'] > 10000)]['nb'].sum()/df_nissan['nb'].sum()
    List_final_name = df_nissan[(df_nissan['nb'] > 10000)].sort_values('nb', ascending=False)['new_nom_commercial'].to_list()
    df_nissan.loc[df_nissan['new_nom_commercial'].isin(List_final_name) == False, 'new_nom_commercial'] = 'DIVERS'
    return(df_nissan)

def MBK(df_to_clean):
    df_mbk = df_to_clean[df_to_clean['Equiv'] == 'MBK'].copy()
    df_mbk['new_nom_commercial'] = df_mbk['nom_commercial']
    df_mbk.loc[df_mbk['nb'].astype(str).str.isnumeric() == False, 'new_nom_commercial'] = \
        df_mbk[df_mbk['nb'].astype(str).str.isnumeric() == False]['new_nom_commercial'].astype(str)+ \
        df_mbk[df_mbk['nb'].astype(str).str.isnumeric() == False]['nb']
    df_mbk.loc[df_mbk['nb'].astype(str).str.isnumeric() == False, 'nb'] = 1
    df_mbk.loc[df_mbk['new_nom_commercial'] == 'XLIMIT', 'new_nom_commercial'] = 'X LIMIT'
    df_mbk.loc[df_mbk['new_nom_commercial'] == 'X-LIMIT SM', 'new_nom_commercial'] = 'X LIMIT'
    df_mbk.loc[df_mbk['new_nom_commercial'] == 'X.LIMIT', 'new_nom_commercial'] = 'X LIMIT'
    df_mbk.loc[df_mbk['new_nom_commercial'] == 'X POWER', 'new_nom_commercial'] = 'X-POWER'
    df_mbk.loc[df_mbk['new_nom_commercial'] == 'XPOWER', 'new_nom_commercial'] = 'X-POWER'
    df_mbk.loc[df_mbk['new_nom_commercial'] == 'X POWER 07', 'new_nom_commercial'] = 'X-POWER'
    df_mbk.loc[df_mbk['new_nom_commercial'] == 'X.POWER', 'new_nom_commercial'] = 'X-POWER'
    df_mbk.loc[df_mbk['new_nom_commercial'] == 'MACH G', 'new_nom_commercial'] = 'MACHG'
    df_mbk.loc[df_mbk['new_nom_commercial'] == 'MACH.G', 'new_nom_commercial'] = 'MACHG'
    df_mbk.loc[df_mbk['new_nom_commercial'] == 'MACH-G', 'new_nom_commercial'] = 'MACHG'
    df_mbk.loc[df_mbk['new_nom_commercial'] == 'MACH', 'new_nom_commercial'] = 'MACHG'
    L_modeles=['BOOSTER', 'STUNT', 'NITRO', 'ROCKET', 'OVETTO', 'BOOSSP', 'YN50', 'XLIMIT', 'SPIRIT', 'DAKOTA', 'X LIMIT',
               'FLIPPER', 'CS50', 'MAGNUM', 'MACH G', 'MACHG', 'X.LIMIT', 'LMP21C10', 'YP250R', 'CW50RS', 'DAYTONA',
               'DERBY', 'HARD ROCK', 'EVOLIS', '51', 'SOLEX']
    df_mbk['new_nom_commercial'] = df_mbk['new_nom_commercial'].apply(detec_name, args=(L_modeles,))
    df_mbk.sort_values('nb', ascending=False)
    df_mbk[(df_mbk['nb'] > 1000)]['nb'].sum()/df_mbk['nb'].sum()
    List_final_name = df_mbk[(df_mbk['nb'] > 1000)].sort_values('nb', ascending=False)['new_nom_commercial'].to_list()
    df_mbk.loc[df_mbk['new_nom_commercial'].isin(List_final_name) == False, 'new_nom_commercial'] = 'DIVERS'
    return(df_mbk)

def OPEL(df_to_clean):
    df_opel = df_to_clean[df_to_clean['Equiv'] == 'OPEL'].copy()
    df_opel['new_nom_commercial'] = df_opel['nom_commercial']
    df_opel.loc[df_opel['nb'].astype(str).str.isnumeric() == False,'new_nom_commercial'] = \
        df_opel[df_opel['nb'].astype(str).str.isnumeric() == False]['new_nom_commercial'].astype(str)+\
        df_opel[df_opel['nb'].astype(str).str.isnumeric() == False]['nb']
    df_opel.loc[df_opel['nb'].astype(str).str.isnumeric() == False,'nb'] = 1
    df_opel.loc[df_opel['new_nom_commercial'] == 'ZAF.', 'new_nom_commercial'] = 'ZAFIRA'
    L_modeles = ['CORSA', 'ASTRA', 'MERIVA', 'ZAFIRA', 'TIGRA', 'CROSSLAND', 'VECTRA', 'VIVARO', 'COMBO', 'INSIGNIA',
                 'OMEGA', 'MOVANO', 'CALIBRA', 'AGILA', 'KADETT', 'ZAF.', 'FOURGON', 'ASCONA', 'COSMO', 'ELEG2', 'ENJOY',
                 'VIV F2700', 'MANTA', 'FASHION', 'CARGO', 'VIV F2900', 'COMFORT', 'ESSENTIA','ELEG', 'SINTRA', 'ANTARA',
                 'SPORT', 'FRONTERA', 'MONZA', 'FOURG']

    df_opel['new_nom_commercial'] = df_opel['new_nom_commercial'].apply(detec_name, args=(L_modeles,))
    df_opel.sort_values('nb', ascending=False)
    df_opel[(df_opel['nb'] > 5000)]['nb'].sum()/df_opel['nb'].sum()
    List_final_name = df_opel[(df_opel['nb'] > 5000)].sort_values('nb', ascending=False)['new_nom_commercial'].to_list()
    df_opel.loc[df_opel['new_nom_commercial'].isin(List_final_name) == False, 'new_nom_commercial']='DIVERS'
    return(df_opel)

def HONDA(df_to_clean):
    df_honda = df_to_clean[df_to_clean['Equiv'] == 'HONDA'].copy()
    df_honda['new_nom_commercial'] = df_honda['nom_commercial']
    df_honda.loc[df_honda['nb'].astype(str).str.isnumeric() == False,'new_nom_commercial'] = \
        df_honda[df_honda['nb'].astype(str).str.isnumeric() == False]['new_nom_commercial'].astype(str)+\
        df_honda[df_honda['nb'].astype(str).str.isnumeric() == False]['nb']
    df_honda.loc[df_honda['nb'].astype(str).str.isnumeric() == False,'nb'] = 1
    L_modeles=['CIVIC','CBR','CRF','CBF','VFR','CB1000','NSC110','NT700','NC700','NC750','ACCORD','CB1100','PRELUDE','CB500','INTEGRA',
              'CB600','CB650','CONCERTO','FJS400','CTX','CMX','GL','ST','CB 600','CB750','VT 125']
    df_honda['new_nom_commercial'] = df_honda['new_nom_commercial'].apply(detec_name, args=(L_modeles,))
    df_honda = df_honda.groupby(['Equiv','nom_commercial','new_nom_commercial']).agg({'nb': 'sum'}).reset_index()
    df_honda[df_honda['nb']>1000].sort_values('nb',ascending=False).tail(60)
    df_honda[(df_honda['nb']>2000)]['nb'].sum()/df_honda['nb'].sum()
    List_final_name = df_honda[(df_honda['nb']>2000)].sort_values('nb', ascending=False)['new_nom_commercial'].to_list()
    df_honda.loc[df_honda['new_nom_commercial'].isin(List_final_name) == False, 'new_nom_commercial'] = 'DIVERS'
    df_honda['new_nom_commercial'].nunique()
    return(df_honda)

def ALFA_ROMEO(df_to_clean):
    df_alfa = df_to_clean[df_to_clean['Equiv'] == 'ALFA ROMEO'].copy()
    df_alfa['new_nom_commercial'] = df_alfa['nom_commercial']
    df_alfa.loc[df_alfa['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial'] = \
        df_alfa[df_alfa['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+\
        df_alfa[df_alfa['nb'].astype(str).str.isnumeric()==False]['nb']
    df_alfa.loc[df_alfa['nb'].astype(str).str.isnumeric()==False,'nb'] = 1
    df_alfa['new_nom_commercial'] = df_alfa['new_nom_commercial'].apply(detec_number, args=(3,))
    df_alfa['new_nom_commercial'] = df_alfa['new_nom_commercial'].apply(detec_number, args=(4,))
    df_alfa.loc[df_alfa['new_nom_commercial']=='ALFA75','new_nom_commercial']='ALFA 75'
    df_alfa.loc[df_alfa['new_nom_commercial']=='ALFA33','new_nom_commercial']='ALFA 33'
    df_alfa.loc[df_alfa['new_nom_commercial']=='JTD 150 5P','new_nom_commercial']='150'
    df_alfa.loc[df_alfa['new_nom_commercial']=='JTD 150 3P','new_nom_commercial']='150'
    df_alfa.loc[df_alfa['new_nom_commercial']=='JTD 150 5P DISTINCTIVE','new_nom_commercial']='150'
    df_alfa.loc[df_alfa['new_nom_commercial']=='JTD 150 3P DISTINCTIVE','new_nom_commercial']='150'
    df_alfa.loc[df_alfa['new_nom_commercial']=='JTD 140 5P','new_nom_commercial']='140'
    df_alfa.loc[df_alfa['new_nom_commercial']=='140 DIST','new_nom_commercial']='140'
    df_alfa.loc[df_alfa['new_nom_commercial']=='SW 1.9JTD','new_nom_commercial']='SW 1.9 JTD'
    df_alfa.loc[df_alfa['new_nom_commercial']=='JTD 126 5P DISTINCTIVE','new_nom_commercial']='126'
    df_alfa['new_nom_commercial']=df_alfa['new_nom_commercial'].replace('ALFA ROMEO','ALFA',regex=True)
    df_alfa['new_nom_commercial']=df_alfa['new_nom_commercial'].replace('ALFA SW JTD','ALFA',regex=True)
    df_alfa['new_nom_commercial']=df_alfa['new_nom_commercial'].replace('ALPHA','ALFA',regex=True)
    df_alfa['new_nom_commercial']=df_alfa['new_nom_commercial'].replace('ALFA ','',regex=True)
    df_alfa['new_nom_commercial']=df_alfa['new_nom_commercial'].replace('ALFA146TD','146',regex=True)
    L_modeles=['GIULIA','GIULIETTA','GT','SPIDER','MITO','ALFA75','ALFA33','ALFASUD','ALFA 146','ALFA 156','ALFA 166','ALFA 159','BRERA',
              'ALFETTA','ALFA 33','ALFA 157','ALFA 75','ALFA 126','CROSSWAGON']
    df_alfa['new_nom_commercial']= df_alfa['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_alfa = df_alfa.groupby(['Equiv', 'nom_commercial', 'new_nom_commercial']).agg({'nb': 'sum'}).reset_index()
    df_alfa[df_alfa['nb'] > 50].sort_values('nb', ascending=False)
    df_alfa[(df_alfa['nb'] > 5000)]['nb'].sum()/df_alfa['nb'].sum()
    List_final_name=df_alfa[(df_alfa['nb']>5000)].sort_values('nb', ascending=False)['new_nom_commercial'].to_list()
    df_alfa.loc[df_alfa['new_nom_commercial'].isin(List_final_name) == False, 'new_nom_commercial'] = 'DIVERS'
    df_alfa['new_nom_commercial'].nunique()
    return(df_alfa)

def FORD(df_to_clean):
    df_ford = df_to_clean[df_to_clean['Equiv'] == 'FORD'].copy()
    df_ford['new_nom_commercial'] = df_ford['nom_commercial']
    df_ford.loc[df_ford['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_ford[df_ford['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_ford[df_ford['nb'].astype(str).str.isnumeric()==False]['nb']
    df_ford.loc[df_ford['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_ford['new_nom_commercial']= df_ford['new_nom_commercial'].apply(detec_number,args=(3,))
    df_ford['new_nom_commercial']= df_ford['new_nom_commercial'].apply(detec_number,args=(4,))
    L_modeles=['TRANSIT','FOCUS','FIESTA','MONDEO','TOURNEO','GALAXY','ESCORT','SIERRA','MUSTANG','ORION','SCORPIO',
               'COURRIER','RAPTOR','PUMA','COUGAR','PROBE','FUSION','KA','MAJOR','B-MAX','PICKUP','COBRA','CAPRI','FLASH',
               'TONUS','GRANADA','F-150','CONNECT','DEXTA']
    df_ford['new_nom_commercial'] = df_ford['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_ford[df_ford['nb'] > 50].sort_values('nb',ascending=False).head(60)
    df_ford[(df_ford['nb'] > 10000)]['nb'].sum()/df_ford['nb'].sum()
    List_final_name = df_ford[(df_ford['nb']>10000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_ford.loc[df_ford['new_nom_commercial'].isin(List_final_name) == False, 'new_nom_commercial'] ='DIVERS'
    df_ford['new_nom_commercial'].nunique()
    return(df_ford)

def BMW(df_to_clean):
    df_bmw = df_to_clean[df_to_clean['Equiv']=='BMW'].copy()
    df_bmw['new_nom_commercial']=df_bmw['nom_commercial']
    df_bmw.loc[df_bmw['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_bmw[df_bmw['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_bmw[df_bmw['nb'].astype(str).str.isnumeric()==False]['nb']
    df_bmw.loc[df_bmw['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_bmw['new_nom_commercial']= df_bmw['new_nom_commercial'].apply(detec_number,args=(3,))
    df_bmw.loc[df_bmw['new_nom_commercial']=='SERIE1','new_nom_commercial']='SERIE 1'
    df_bmw.loc[df_bmw['new_nom_commercial']=='SERIE3','new_nom_commercial']='SERIE 3'
    df_bmw.loc[df_bmw['new_nom_commercial']=='SERIE5','new_nom_commercial']='SERIE 5'
    df_bmw.loc[df_bmw['new_nom_commercial']=='SERIE6','new_nom_commercial']='SERIE 6'
    df_bmw.loc[df_bmw['new_nom_commercial']=='SERIE I','new_nom_commercial']='SERIE 1'
    L_modeles=['R 1250','X1','X3','R 1200','X5','K 1600','S 1000','X2','F 800', 'F 750','C650','F 800','F 650','F 900','F 700','X4',
              'F 850','C 400','216D', '218D','R NINE','X6','M340','K1300','M440','Z4','R1100','K1200','SERIE X','R100R','R1200','M4','M3',
              'SERIE 3','R 18','SERIE 2','SERIE 1','SERIE 4','SERIE 5','SERIE 7','X7','M2','K1600','K100','K 1300']

    df_bmw['new_nom_commercial']= df_bmw['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_bmw[df_bmw['nb']>50].sort_values('nb',ascending=False).head(60)
    df_bmw[(df_bmw['nb']>10000)]['nb'].sum()/df_bmw['nb'].sum()
    List_final_name=df_bmw[(df_bmw['nb']>10000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_bmw.loc[df_bmw['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    df_bmw['new_nom_commercial'].nunique()
    return(df_bmw)

def AUDI(df_to_clean):
    df_audi = df_to_clean[df_to_clean['Equiv']=='AUDI'].copy()
    df_audi['new_nom_commercial']=df_audi['nom_commercial']
    df_audi.loc[df_audi['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_audi[df_audi['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_audi[df_audi['nb'].astype(str).str.isnumeric()==False]['nb']
    df_audi.loc[df_audi['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_audi['new_nom_commercial']= df_audi['new_nom_commercial'].apply(detec_number,args=(3,))
    df_audi.loc[df_audi['new_nom_commercial']=='A 4','new_nom_commercial']='A4'
    df_audi.loc[df_audi['new_nom_commercial']=='RS 6','new_nom_commercial']='RS6'
    df_audi.loc[df_audi['new_nom_commercial']=='RS 4','new_nom_commercial']='RS4'
    df_audi.loc[df_audi['new_nom_commercial']=='RS 7','new_nom_commercial']='RS7'

    L_modeles=['A1','A2','A3','A4','A5','A6','Q2','Q3','Q5','Q7','Q8','A7','RS 3','RS4','RS5','RS6','RS 6','S1','S3','S5','S4','S6','TT','Q4',
              'R8','S7','E-TRON','ALLROAD','RS4','RS7','1002','TURBO','COUPE','CABRIOLET','A8','802','80CC','801']

    df_audi['new_nom_commercial']= df_audi['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_audi[df_audi['nb']>50].sort_values('nb',ascending=False).head(60)
    df_audi[(df_audi['nb']>10000)]['nb'].sum()/df_audi['nb'].sum()
    List_final_name=df_audi[(df_audi['nb']>10000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_audi.loc[df_audi['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    df_audi['new_nom_commercial'].nunique()
    return(df_audi)

def LANCIA(df_to_clean):
    df_lancia=df_to_clean[df_to_clean['Equiv']=='LANCIA'].copy()
    df_lancia['new_nom_commercial']=df_lancia['nom_commercial']
    df_lancia.loc[df_lancia['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_lancia[df_lancia['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_lancia[df_lancia['nb'].astype(str).str.isnumeric()==False]['nb']
    df_lancia.loc[df_lancia['nb'].astype(str).str.isnumeric()==False,'nb']=1

    df_lancia.loc[df_lancia['new_nom_commercial']=='YLE','new_nom_commercial']='YPSILON'
    df_lancia.loc[df_lancia['new_nom_commercial']=='YLE/LS','new_nom_commercial']='YPSILON'
    df_lancia.loc[df_lancia['new_nom_commercial']=='YLS','new_nom_commercial']='YPSILON'
    df_lancia.loc[df_lancia['new_nom_commercial']=='YLX','new_nom_commercial']='YPSILON'
    df_lancia.loc[df_lancia['new_nom_commercial']=='Y1.2','new_nom_commercial']='LANCIA Y'
    df_lancia.loc[df_lancia['new_nom_commercial']=='Y1.4','new_nom_commercial']='LANCIA Y'
    df_lancia.loc[df_lancia['new_nom_commercial']=='Y','new_nom_commercial']='LANCIA Y'
    df_lancia.loc[df_lancia['new_nom_commercial']=='Y1.3JTD','new_nom_commercial']='LANCIA Y'
    L_modeles=['DELTA','LYBRA','Y10','BETA','KAPPA','ZETA','YPSILON','DEDRA','PHEDRA','FULVIA','MUSA','FLAVIA','THEMA','THESIS']
    df_lancia['new_nom_commercial']= df_lancia['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_lancia[df_lancia['nb']>50].sort_values('nb',ascending=False).head(60)
    df_lancia[(df_lancia['nb']>700)]['nb'].sum()/df_lancia['nb'].sum()
    List_final_name=df_lancia[(df_lancia['nb']>700)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_lancia.loc[df_lancia['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    df_lancia['new_nom_commercial'].nunique()
    return(df_lancia)

def TOYOTA(df_to_clean):
    df_toyota=df_to_clean[df_to_clean['Equiv']=='TOYOTA'].copy()
    df_toyota['new_nom_commercial']=df_toyota['nom_commercial']
    df_toyota.loc[df_toyota['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_toyota[df_toyota['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_toyota[df_toyota['nb'].astype(str).str.isnumeric()==False]['nb']
    df_toyota.loc[df_toyota['nb'].astype(str).str.isnumeric()==False,'nb']=1

    df_toyota.loc[df_toyota['new_nom_commercial']=='L.CRUISER','new_nom_commercial']='LAND CRUISER'
    df_toyota.loc[df_toyota['new_nom_commercial']=='LAND CRUI','new_nom_commercial']='LAND CRUISER'
    df_toyota.loc[df_toyota['new_nom_commercial']=='LANDCRUISER','new_nom_commercial']='LAND CRUISER'
    df_toyota.loc[df_toyota['new_nom_commercial']=='LAND CRUIS','new_nom_commercial']='LAND CRUISER'
    df_toyota.loc[df_toyota['new_nom_commercial']=='L. CRUISER','new_nom_commercial']='LAND CRUISER'
    df_toyota.loc[df_toyota['new_nom_commercial']=='F.CRUISER','new_nom_commercial']='FJ CRUISER'
    df_toyota.loc[df_toyota['new_nom_commercial']=='HI ACE','new_nom_commercial']='HI-ACE'
    df_toyota.loc[df_toyota['new_nom_commercial']=='HIACE','new_nom_commercial']='HI-ACE'

    L_modeles=['YARIS','COROLLA','AYGO','C-HR','RAV4','PRIUS','CAMRY','LAND CRUISER','HILUX','L.CRUISER','DYNA','VERSO','PROACE','AVENSIS',
              'CARINA','RUNNER','PASEO','SUPRA','CELICA','HI-ACE','PREVIA','MIRAI']
    df_toyota['new_nom_commercial']= df_toyota['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_toyota[df_toyota['nb']>50].sort_values('nb',ascending=False).head(60)
    df_toyota[(df_toyota['nb']>10000)]['nb'].sum()/df_toyota['nb'].sum()
    List_final_name=df_toyota[(df_toyota['nb']>10000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_toyota.loc[df_toyota['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_toyota)

def SEAT(df_to_clean):
    df_seat=df_to_clean[df_to_clean['Equiv']=='SEAT'].copy()
    df_seat['new_nom_commercial']=df_seat['nom_commercial']
    df_seat.loc[df_seat['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_seat[df_seat['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_seat[df_seat['nb'].astype(str).str.isnumeric()==False]['nb']
    df_seat.loc[df_seat['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['IBIZA','CORDOBA','LEON','EXEO','ATECA','ALTEA','TOLEDO','ALHAMBRA','AROSA','VARIO','MALAGA','CUPRA']

    df_seat['new_nom_commercial']= df_seat['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_seat[df_seat['nb']>50].sort_values('nb',ascending=False).head(60)
    df_seat[(df_seat['nb']>10000)]['nb'].sum()/df_seat['nb'].sum()
    List_final_name=df_seat[(df_seat['nb']>10000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_seat.loc[df_seat['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_seat)

def VOLVO(df_to_clean):
    df_volvo=df_to_clean[df_to_clean['Equiv']=='VOLVO'].copy()
    df_volvo['new_nom_commercial']=df_volvo['nom_commercial']
    df_volvo.loc[df_volvo['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_volvo[df_volvo['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_volvo[df_volvo['nb'].astype(str).str.isnumeric()==False]['nb']
    df_volvo.loc[df_volvo['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_volvo.loc[df_volvo['new_nom_commercial']=='XC 90','new_nom_commercial']='XC90'
    df_volvo.loc[df_volvo['new_nom_commercial']=='XC 60','new_nom_commercial']='XC60'
    df_volvo.loc[df_volvo['new_nom_commercial']=='740740','new_nom_commercial']='740'
    df_volvo.loc[df_volvo['new_nom_commercial']=='245245','new_nom_commercial']='245'
    df_volvo.loc[df_volvo['new_nom_commercial']=='760760','new_nom_commercial']='760'

    L_modeles=['V40','V60','V90','XC60','XC90','XC40','440','460','480','FH','S80','S401','S60','850GL','940GL','V70','S90','S70','960GL','XC70',
              'ROLFO','FL6','940','850T']
    df_volvo['new_nom_commercial']= df_volvo['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_volvo[df_volvo['nb']>50].sort_values('nb',ascending=False).head(60)
    df_volvo[(df_volvo['nb']>10000)]['nb'].sum()/df_volvo['nb'].sum()
    List_final_name=df_volvo[(df_volvo['nb']>10000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_volvo.loc[df_volvo['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_volvo)

def TRIGANO(df_to_clean):
    df_trigano=df_to_clean[df_to_clean['Equiv']=='TRIGANO'].copy()
    df_trigano['new_nom_commercial']=df_trigano['nom_commercial']
    df_trigano.loc[df_trigano['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_trigano[df_trigano['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_trigano[df_trigano['nb'].astype(str).str.isnumeric()==False]['nb']
    df_trigano.loc[df_trigano['nb'].astype(str).str.isnumeric()==False,'nb']=1

    df_trigano.loc[df_trigano['new_nom_commercial']=='RB 2524/2524SF','new_nom_commercial']='RB2524'
    df_trigano.loc[df_trigano['new_nom_commercial']=='RB 2524','new_nom_commercial']='RB2524'
    df_trigano.loc[df_trigano['new_nom_commercial']=='AB 254','new_nom_commercial']='ABE 254'
    df_trigano.loc[df_trigano['new_nom_commercial']=='VBE893','new_nom_commercial']='VBE 893'

    L_modeles=['SILVER','RB2524','NFB','PVB','RUBIS','ABE','REV','EMERAUDE','RX']
    df_trigano['new_nom_commercial']= df_trigano['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_trigano[df_trigano['nb']>50].sort_values('nb',ascending=False).head(60)
    List_final_name=df_trigano[(df_trigano['nb']>50)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_trigano.loc[df_trigano['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_trigano)

def PORSCHE(df_to_clean):
    df_porsche=df_to_clean[df_to_clean['Equiv']=='PORSCHE'].copy()
    df_porsche['new_nom_commercial']=df_porsche['nom_commercial']
    df_porsche.loc[df_porsche['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_porsche[df_porsche['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_porsche[df_porsche['nb'].astype(str).str.isnumeric()==False]['nb']
    df_porsche.loc[df_porsche['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['CAYENNE','MACAN','911','PANAMERA','BOXTER','CAYMAN','TAYCAN','944','928','924','914','996','CARRERA','993','356','918','997',
              '968','987','997','991']
    df_porsche['new_nom_commercial']= df_porsche['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_porsche[df_porsche['nb']>50].sort_values('nb',ascending=False).head(60)
    df_porsche[(df_porsche['nb']>1000)]['nb'].sum()/df_porsche['nb'].sum()
    List_final_name=df_porsche[(df_porsche['nb']>1000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_porsche.loc[df_porsche['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_porsche)

def SKODA(df_to_clean):
    df_skoda=df_to_clean[df_to_clean['Equiv']=='SKODA'].copy()
    df_skoda['new_nom_commercial']=df_skoda['nom_commercial']
    df_skoda.loc[df_skoda['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_skoda[df_skoda['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_skoda[df_skoda['nb'].astype(str).str.isnumeric()==False]['nb']
    df_skoda.loc[df_skoda['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['FABIA','OCTAVIA','ROOMSTER','SAFARI','ENYAQ','TOURMALIN','LD SB','OPALIN','SUPERB','FELICIA','BOX','OPAL']

    df_skoda['new_nom_commercial']= df_skoda['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_skoda[df_skoda['nb']>50].sort_values('nb',ascending=False).head(60)
    df_skoda[(df_skoda['nb']>10000)]['nb'].sum()/df_skoda['nb'].sum()
    List_final_name=df_skoda[(df_skoda['nb']>10000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_skoda.loc[df_skoda['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_skoda)

def TRIUMPH(df_to_clean):
    df_triumph=df_to_clean[df_to_clean['Equiv']=='TRIUMPH'].copy()
    df_triumph['new_nom_commercial']=df_triumph['nom_commercial']
    df_triumph.loc[df_triumph['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_triumph[df_triumph['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_triumph[df_triumph['nb'].astype(str).str.isnumeric()==False]['nb']
    df_triumph.loc[df_triumph['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['STREET TRIPLE','BONNEVILLE','TIGER','DAYTONA','SPEED TRIPLE','SCRAMBLER','ROCKET','THRUXTON','THUNDERBIRD','TROPHY',
              'SPITFIRE','EXPLORER','SPRINT']
    df_triumph['new_nom_commercial']= df_triumph['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_triumph[df_triumph['nb']>50].sort_values('nb',ascending=False).head(60)
    df_triumph[(df_triumph['nb']>1000)]['nb'].sum()/df_triumph['nb'].sum()
    List_final_name=df_triumph[(df_triumph['nb']>1000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_triumph.loc[df_triumph['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_triumph)

def MITSUBISHI(df_to_clean):
    df_mitsu=df_to_clean[df_to_clean['Equiv']=='MITSUBISHI'].copy()
    df_mitsu['new_nom_commercial']=df_mitsu['nom_commercial']
    df_mitsu.loc[df_mitsu['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_mitsu[df_mitsu['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_mitsu[df_mitsu['nb'].astype(str).str.isnumeric()==False]['nb']
    df_mitsu.loc[df_mitsu['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_mitsu.loc[df_mitsu['new_nom_commercial']=='L 200','new_nom_commercial']='L200'
    L_modeles=['PAJERO','SPACE STAR','L200','OUTLANDER','ECLIPSE CROSS']
    df_mitsu['new_nom_commercial']= df_mitsu['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_mitsu[df_mitsu['nb']>50].sort_values('nb',ascending=False).head(60)
    df_mitsu[(df_mitsu['nb']>8000)]['nb'].sum()/df_mitsu['nb'].sum()
    List_final_name=df_mitsu[(df_mitsu['nb']>8000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_mitsu.loc[df_mitsu['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_mitsu)

def ROVER(df_to_clean):
    df_rover=df_to_clean[df_to_clean['Equiv']=='ROVER'].copy()
    df_rover['new_nom_commercial']=df_rover['nom_commercial']
    df_rover.loc[df_rover['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_rover[df_rover['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_rover[df_rover['nb'].astype(str).str.isnumeric()==False]['nb']
    df_rover.loc[df_rover['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_rover.loc[df_rover['new_nom_commercial']=='200','new_nom_commercial']='SERIE 200'
    L_modeles=['200214','200218','600620','MINI','400420','100111','400414','2002116']

    df_rover['new_nom_commercial']= df_rover['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_rover[df_rover['nb']>50].sort_values('nb',ascending=False).head(60)
    df_rover[(df_rover['nb']>500)]['nb'].sum()/df_rover['nb'].sum()
    List_final_name=df_rover[(df_rover['nb']>500)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_rover.loc[df_rover['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_rover

def CHEVROLET(df_to_clean):
    df_chevrolet=df_to_clean[df_to_clean['Equiv']=='CHEVROLET'].copy()
    df_chevrolet['new_nom_commercial']=df_chevrolet['nom_commercial']
    df_chevrolet.loc[df_chevrolet['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_chevrolet[df_chevrolet['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_chevrolet[df_chevrolet['nb'].astype(str).str.isnumeric()==False]['nb']
    df_chevrolet.loc[df_chevrolet['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['200214','200218','600620','MINI','400420','100111','400414','2002116']

    df_chevrolet['new_nom_commercial']= df_chevrolet['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    List_final_name=df_chevrolet[(df_chevrolet['nb']>5000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_chevrolet.loc[df_chevrolet['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'

    return(df_chevrolet)

def JAGUAR(df_to_clean):
    df_jaguar=df_to_clean[df_to_clean['Equiv']=='JAGUAR'].copy()
    df_jaguar['new_nom_commercial']=df_jaguar['nom_commercial']
    df_jaguar.loc[df_jaguar['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_jaguar[df_jaguar['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_jaguar[df_jaguar['nb'].astype(str).str.isnumeric()==False]['nb']
    df_jaguar.loc[df_jaguar['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['EVOQUE','DISCOVERY','XK8','XK','SOVEREIGN','XKR','XF','E-PACE','I-PACE','F-TYPE','F-PACE','XJ','XE','RANGE ROVER','X-TYPE',
              'S-TYPE','TYPE E']
    df_jaguar['new_nom_commercial']= df_jaguar['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    List_final_name=df_jaguar[(df_jaguar['nb']>5000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_jaguar.loc[df_jaguar['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_jaguar

def DODGE(df_to_clean):
    df_dodge=df_to_clean[df_to_clean['Equiv']=='DODGE'].copy()
    df_dodge['new_nom_commercial']=df_dodge['nom_commercial']
    df_dodge.loc[df_dodge['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_dodge[df_dodge['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_dodge[df_dodge['nb'].astype(str).str.isnumeric()==False]['nb']
    df_dodge.loc[df_dodge['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_dodge.loc[df_dodge['new_nom_commercial']=='RAM1500','new_nom_commercial']='RAM 1500'
    L_modeles=['RAM 1500','CHALLENGER','JOURNEY','VIPER','PICK UP']
    df_dodge['new_nom_commercial']= df_dodge['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_dodge[df_dodge['nb']>50].sort_values('nb',ascending=False).head(60)
    df_dodge[(df_dodge['nb']>500)]['nb'].sum()/df_dodge['nb'].sum()

    List_final_name=df_dodge[(df_dodge['nb']>500)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_dodge.loc[df_dodge['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_dodge

def MAZDA(df_to_clean):
    df_mazda=df_to_clean[df_to_clean['Equiv']=='MAZDA'].copy()
    df_mazda['new_nom_commercial']=df_mazda['nom_commercial']
    df_mazda.loc[df_mazda['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_mazda[df_mazda['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_mazda[df_mazda['nb'].astype(str).str.isnumeric()==False]['nb']
    df_mazda.loc[df_mazda['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_mazda.loc[df_mazda['new_nom_commercial']=='MAZDA2','new_nom_commercial']='MAZDA 2'
    df_mazda.loc[df_mazda['new_nom_commercial']=='MAZDA3','new_nom_commercial']='MAZDA 3'
    df_mazda.loc[df_mazda['new_nom_commercial']=='MAZDA6','new_nom_commercial']='MAZDA 6'
    df_mazda.loc[df_mazda['new_nom_commercial']=='MX5','new_nom_commercial']='MX-5'
    df_mazda['new_nom_commercial']=df_mazda['new_nom_commercial'].replace('MAZDA ','',regex=True)
    L_modeles=['3231','6261','6262']
    df_mazda['new_nom_commercial']= df_mazda['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_mazda[df_mazda['nb']>50].sort_values('nb',ascending=False).head(60)
    df_mazda[(df_mazda['nb']>5000)]['nb'].sum()/df_mazda['nb'].sum()
    List_final_name=df_mazda[(df_mazda['nb']>5000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_mazda.loc[df_mazda['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_mazda

def LAND_ROVER(df_to_clean):
    df_lr=df_to_clean[df_to_clean['Equiv']=='LAND ROVER'].copy()
    df_lr['new_nom_commercial']=df_lr['nom_commercial']
    df_lr.loc[df_lr['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_lr[df_lr['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_lr[df_lr['nb'].astype(str).str.isnumeric()==False]['nb']
    df_lr.loc[df_lr['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['EVOQUE','DEFENDER','DISCOVERY','R.ROVER','FREELAND','RANGE ROVER SPORT','L.ROVER']
    df_lr['new_nom_commercial']= df_lr['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_lr[df_lr['nb']>50].sort_values('nb',ascending=False).head(60)
    df_lr[(df_lr['nb']>10000)]['nb'].sum()/df_lr['nb'].sum()
    List_final_name=df_lr[(df_lr['nb']>10000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_lr.loc[df_lr['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_lr)

def VESPA(df_to_clean):
    df_vespa=df_to_clean[df_to_clean['Equiv']=='VESPA'].copy()
    df_vespa['new_nom_commercial']=df_vespa['nom_commercial']
    df_vespa.loc[df_vespa['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_vespa[df_vespa['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_vespa[df_vespa['nb'].astype(str).str.isnumeric()==False]['nb']
    df_vespa.loc[df_vespa['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['PIAGGIO','VESPA 50','VESPA 125','PRIMAVERA','CIAO','ACMA','SCOOTER','50','125','150']
    df_vespa['new_nom_commercial']= df_vespa['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_vespa[df_vespa['nb']>0].sort_values('nb',ascending=False).head(60)
    List_final_name=df_vespa[(df_vespa['nb']>50)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_vespa.loc[df_vespa['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_vespa

def FERRARI(df_to_clean):
    df_ferrari=df_to_clean[df_to_clean['Equiv']=='FERRARI'].copy()
    df_ferrari['new_nom_commercial']=df_ferrari['nom_commercial']
    df_ferrari.loc[df_ferrari['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_ferrari[df_ferrari['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_ferrari[df_ferrari['nb'].astype(str).str.isnumeric()==False]['nb']
    df_ferrari.loc[df_ferrari['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['308','812','F8','360','355','328','599','TESTAROS','512','456','488','348','550','MONDIAL','PORTOFINO']
    df_ferrari['new_nom_commercial']= df_ferrari['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_ferrari[df_ferrari['nb']>0].sort_values('nb',ascending=False).head(60)
    List_final_name=df_ferrari[(df_ferrari['nb']>150)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_ferrari.loc[df_ferrari['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_ferrari)

def MINI(df_to_clean):
    df_mini=df_to_clean[df_to_clean['Equiv']=='MINI'].copy()
    df_mini['new_nom_commercial']=df_mini['nom_commercial']
    df_mini.loc[df_mini['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_mini[df_mini['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_mini[df_mini['nb'].astype(str).str.isnumeric()==False]['nb']
    df_mini.loc[df_mini['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_mini.loc[df_mini['new_nom_commercial']=='MINI COOPE','new_nom_commercial']='COOPER'
    df_mini.loc[df_mini['new_nom_commercial']=='MINISPECIA','new_nom_commercial']='SPECIAL'

    L_modeles=['COOPER','ONE','1000','MAYFAI','SPRITE']
    df_mini['new_nom_commercial']= df_mini['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_mini[df_mini['nb']>0].sort_values('nb',ascending=False).head(60)
    List_final_name=df_mini[(df_mini['nb']>1000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_mini.loc[df_mini['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_mini)

def LAMBORGHINI(df_to_clean):
    df_lambo=df_to_clean[df_to_clean['Equiv']=='LAMBORGHINI'].copy()
    df_lambo['new_nom_commercial']=df_lambo['nom_commercial']
    df_lambo.loc[df_lambo['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_lambo[df_lambo['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_lambo[df_lambo['nb'].astype(str).str.isnumeric()==False]['nb']
    df_lambo.loc[df_lambo['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['DIABLO','EVO','REKORD']
    df_lambo['new_nom_commercial']= df_lambo['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_lambo[df_lambo['nb']>0].sort_values('nb',ascending=False).head(60)
    List_final_name=df_lambo[(df_lambo['nb']>50)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_lambo.loc[df_lambo['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_lambo)

def ISUZU(df_to_clean):
    df_isu=df_to_clean[df_to_clean['Equiv']=='ISUZU'].copy()
    df_isu['new_nom_commercial']=df_isu['nom_commercial']
    df_isu.loc[df_isu['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_isu[df_isu['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_isu[df_isu['nb'].astype(str).str.isnumeric()==False]['nb']
    df_isu.loc[df_isu['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_isu.loc[df_isu['new_nom_commercial']=='D MAX','new_nom_commercial']='D-MAX'
    df_isu.loc[df_isu['new_nom_commercial']=='DMAX','new_nom_commercial']='D-MAX'
    df_isu.loc[df_isu['new_nom_commercial']=='DMAX 3.0 CREW','new_nom_commercial']='D-MAX'
    df_isu.loc[df_isu['new_nom_commercial']=='DMAX 3.0 SPACE','new_nom_commercial']='D-MAX'
    df_isu.loc[df_isu['new_nom_commercial']=='DMAX 2.5 CREW','new_nom_commercial']='D-MAX'
    df_isu.loc[df_isu['new_nom_commercial']=='DMAX 2.5 SPACE','new_nom_commercial']='D-MAX'
    df_isu.loc[df_isu['new_nom_commercial']=='DMAX 3.0','new_nom_commercial']='D-MAX'
    L_modeles=['D-MAX','NOVO']
    df_isu['new_nom_commercial']= df_isu['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_isu[df_isu['nb']>0].sort_values('nb',ascending=False).head(60)
    List_final_name=df_isu[(df_isu['nb']>100)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_isu.loc[df_isu['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_isu)

def HYUNDAI(df_to_clean):
    df_hyu=df_to_clean[df_to_clean['Equiv']=='HYUNDAI'].copy()
    df_hyu['new_nom_commercial']=df_hyu['nom_commercial']
    df_hyu.loc[df_hyu['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_hyu[df_hyu['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_hyu[df_hyu['nb'].astype(str).str.isnumeric()==False]['nb']
    df_hyu.loc[df_hyu['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['PONY','SANTAFE','ACCENT','IONIQ','LANTRA','TERRACAN','TUCSON']
    df_hyu.loc[df_hyu['new_nom_commercial']=='SANTAFE','new_nom_commercial']='SANTA FE'
    df_hyu['new_nom_commercial']= df_hyu['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_hyu[df_hyu['nb']>0].sort_values('nb',ascending=False).head(60)
    List_final_name=df_hyu[(df_hyu['nb']>5000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_hyu.loc[df_hyu['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_hyu)

def DACIA(df_to_clean):
    df_dacia=df_to_clean[df_to_clean['Equiv']=='DACIA'].copy()
    df_dacia['new_nom_commercial']=df_dacia['nom_commercial']
    df_dacia.loc[df_dacia['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_dacia[df_dacia['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_dacia[df_dacia['nb'].astype(str).str.isnumeric()==False]['nb']
    df_dacia.loc[df_dacia['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['LOGAN','SANDERO','DUSTER']
    df_dacia['new_nom_commercial']= df_dacia['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_dacia[df_dacia['nb']>0].sort_values('nb',ascending=False).head(60)
    List_final_name=df_dacia[(df_dacia['nb']>5000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_dacia.loc[df_dacia['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'

    return(df_dacia)

def CHRYSLER(df_to_clean):
    df_chrys=df_to_clean[df_to_clean['Equiv']=='CHRYSLER'].copy()
    df_chrys['new_nom_commercial']=df_chrys['nom_commercial']
    df_chrys.loc[df_chrys['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_chrys[df_chrys['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_chrys[df_chrys['nb'].astype(str).str.isnumeric()==False]['nb']
    df_chrys.loc[df_chrys['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['CRUISER','VOYAGER']
    df_chrys['new_nom_commercial']= df_chrys['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_chrys[df_chrys['nb']>0].sort_values('nb',ascending=False).head(60)
    List_final_name=df_chrys[(df_chrys['nb']>2000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_chrys.loc[df_chrys['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_chrys)

def AUSTIN(df_to_clean):
    df_austin=df_to_clean[df_to_clean['Equiv']=='AUSTIN'].copy()
    df_austin['new_nom_commercial']=df_austin['nom_commercial']
    df_austin.loc[df_austin['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_austin[df_austin['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_austin[df_austin['nb'].astype(str).str.isnumeric()==False]['nb']
    df_austin.loc[df_austin['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['METRO','MINI']
    df_austin['new_nom_commercial']= df_austin['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_austin[df_austin['nb']>0].sort_values('nb',ascending=False).head(60)
    List_final_name=df_austin[(df_austin['nb']>100)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_austin.loc[df_austin['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_austin)

def SIMCA(df_to_clean):
    df_sim=df_to_clean[df_to_clean['Equiv']=='SIMCA'].copy()
    df_sim['new_nom_commercial']=df_sim['nom_commercial']
    df_sim.loc[df_sim['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_sim[df_sim['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_sim[df_sim['nb'].astype(str).str.isnumeric()==False]['nb']
    df_sim.loc[df_sim['nb'].astype(str).str.isnumeric()==False,'nb']=1

    L_modeles=['1000','1100','90','ARONDE','ARIANE']
    df_sim['new_nom_commercial']= df_sim['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_sim[df_sim['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_sim[(df_sim['nb']>90)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_sim.loc[df_sim['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_sim)

def MG(df_to_clean):
    df_mg=df_to_clean[df_to_clean['Equiv']=='MG'].copy()
    df_mg['new_nom_commercial']=df_mg['nom_commercial']
    df_mg.loc[df_mg['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_mg[df_mg['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_mg[df_mg['nb'].astype(str).str.isnumeric()==False]['nb']
    df_mg.loc[df_mg['nb'].astype(str).str.isnumeric()==False,'nb']=1

    L_modeles=['F','TF','ZR','ZS','RX6','EHS']
    df_mg['new_nom_commercial']= df_mg['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_mg[df_mg['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_mg[(df_mg['nb']>500)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_mg.loc[df_mg['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_mg)

def JEEP(df_to_clean):
    df_jeep=df_to_clean[df_to_clean['Equiv']=='JEEP'].copy()
    df_jeep['new_nom_commercial']=df_jeep['nom_commercial']
    df_jeep.loc[df_jeep['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_jeep[df_jeep['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_jeep[df_jeep['nb'].astype(str).str.isnumeric()==False]['nb']
    df_jeep.loc[df_jeep['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['RENEGADE','CHEROKE','WRANGLER']
    df_jeep['new_nom_commercial']= df_jeep['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_jeep.loc[df_jeep['new_nom_commercial']=='CHEROKE','new_nom_commercial']='CHEROKEE'
    df_jeep[df_jeep['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_jeep[(df_jeep['nb']>1000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_jeep.loc[df_jeep['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_jeep)

def KIA(df_to_clean):
    df_kia=df_to_clean[df_to_clean['Equiv']=='KIA'].copy()
    df_kia['new_nom_commercial']=df_kia['nom_commercial']
    df_kia.loc[df_kia['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_kia[df_kia['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_kia[df_kia['nb'].astype(str).str.isnumeric()==False]['nb']
    df_kia.loc[df_kia['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['CEED']
    df_kia['new_nom_commercial']= df_kia['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_kia.loc[df_kia['new_nom_commercial']=='CEE\'D','new_nom_commercial']='CEED'
    df_kia[df_kia['nb']>0].sort_values('nb',ascending=False).head(20)
    df_kia_annexe=df_kia[['new_nom_commercial','nb']]
    df_kia_annexe=df_kia_annexe.groupby(['new_nom_commercial']).agg({'nb':'sum'}).reset_index()
    df_kia_annexe[df_kia_annexe['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_kia[(df_kia['nb']>10000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_kia.loc[df_kia['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_kia)

def DAIMLER(df_to_clean):
    df_daim=df_to_clean[df_to_clean['Equiv']=='DAIMLER'].copy()
    df_daim['new_nom_commercial']=df_daim['nom_commercial']
    df_daim.loc[df_daim['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_daim[df_daim['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_daim[df_daim['nb'].astype(str).str.isnumeric()==False]['nb']
    df_daim.loc[df_daim['nb'].astype(str).str.isnumeric()==False,'nb']=1

    L_modeles=['DAIMLER','DOUBLE']
    df_daim['new_nom_commercial']= df_daim['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_daim[df_daim['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_daim[(df_daim['nb']>100)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_daim.loc[df_daim['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_daim)

def LOTUS(df_to_clean):
    df_lotus=df_to_clean[df_to_clean['Equiv']=='LOTUS'].copy()
    df_lotus['new_nom_commercial']=df_lotus['nom_commercial']
    df_lotus.loc[df_lotus['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_lotus[df_lotus['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_lotus[df_lotus['nb'].astype(str).str.isnumeric()==False]['nb']
    df_lotus.loc[df_lotus['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['ELISE','EXIGE']
    df_lotus['new_nom_commercial']= df_lotus['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_lotus[df_lotus['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_lotus[(df_lotus['nb']>1000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_lotus.loc[df_lotus['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_lotus)

def GMC(df_to_clean):
    df_gmc=df_to_clean[df_to_clean['Equiv']=='GMC'].copy()
    df_gmc['new_nom_commercial']=df_gmc['nom_commercial']
    df_gmc.loc[df_gmc['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_gmc[df_gmc['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_gmc[df_gmc['nb'].astype(str).str.isnumeric()==False]['nb']
    df_gmc.loc[df_gmc['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['YUKON','SIERRA','HUMMER']
    df_gmc['new_nom_commercial']= df_gmc['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_gmc[df_gmc['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_gmc[(df_gmc['nb']>90)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_gmc.loc[df_gmc['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_gmc)

def MASERATI(df_to_clean):
    df_mas=df_to_clean[df_to_clean['Equiv']=='MASERATI'].copy()
    df_mas['new_nom_commercial']=df_mas['nom_commercial']
    df_mas.loc[df_mas['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_mas[df_mas['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_mas[df_mas['nb'].astype(str).str.isnumeric()==False]['nb']
    df_mas.loc[df_mas['nb'].astype(str).str.isnumeric()==False,'nb']=1

    L_modeles=['GRANTURISMO','QUATTROPORTE','GHIBLI','LEVANTE','GRANCABRIO']
    df_mas['new_nom_commercial']= df_mas['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_mas[df_mas['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_mas[(df_mas['nb']>400)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_mas.loc[df_mas['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_mas

def SMART(df_to_clean):
    df_smart=df_to_clean[df_to_clean['Equiv']=='SMART'].copy()
    df_smart['new_nom_commercial']=df_smart['nom_commercial']
    df_smart.loc[df_smart['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_smart[df_smart['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_smart[df_smart['nb'].astype(str).str.isnumeric()==False]['nb']
    df_smart.loc[df_smart['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['FORTWO','FORFOUR']
    df_smart['new_nom_commercial']= df_smart['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_smart[df_smart['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_smart[(df_smart['nb']>20000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_smart.loc[df_smart['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_smart

def CADILLAC(df_to_clean):
    df_cad=df_to_clean[df_to_clean['Equiv']=='CADILLAC'].copy()
    df_cad['new_nom_commercial']=df_cad['nom_commercial']
    df_cad.loc[df_cad['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_cad[df_cad['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_cad[df_cad['nb'].astype(str).str.isnumeric()==False]['nb']
    df_cad.loc[df_cad['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['ESCALADE','SEVILLE','FLEETWOOD']
    df_cad['new_nom_commercial']= df_cad['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_cad[df_cad['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_cad[(df_cad['nb']>60)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_cad.loc[df_cad['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'

    return df_cad

def LEXUS(df_to_clean):
    df_lex=df_to_clean[df_to_clean['Equiv']=='LEXUS'].copy()
    df_lex['new_nom_commercial']=df_lex['nom_commercial']
    df_lex.loc[df_lex['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_lex[df_lex['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_lex[df_lex['nb'].astype(str).str.isnumeric()==False]['nb']
    df_lex.loc[df_lex['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['NX300H','UX250H']
    df_lex['new_nom_commercial']= df_lex['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_lex[df_lex['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_lex[(df_lex['nb']>2000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_lex.loc[df_lex['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_lex

def BENTLEY(df_to_clean):
    df_ben=df_to_clean[df_to_clean['Equiv']=='BENTLEY'].copy()
    df_ben['new_nom_commercial']=df_ben['nom_commercial']
    df_ben.loc[df_ben['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_ben[df_ben['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_ben[df_ben['nb'].astype(str).str.isnumeric()==False]['nb']
    df_ben.loc[df_ben['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['CONTINENTAL GT']
    df_ben['new_nom_commercial']= df_ben['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_ben[df_ben['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_ben[(df_ben['nb']>1000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_ben.loc[df_ben['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_ben

def LADA(df_to_clean):
    df_lada=df_to_clean[df_to_clean['Equiv']=='LADA'].copy()
    df_lada['new_nom_commercial']=df_lada['nom_commercial']
    df_lada.loc[df_lada['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_lada[df_lada['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_lada[df_lada['nb'].astype(str).str.isnumeric()==False]['nb']
    df_lada.loc[df_lada['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_lada.loc[df_lada['new_nom_commercial']=='LADA 4 X 4','new_nom_commercial']='LADA 4X4'
    L_modeles=['LADA 4X4']
    df_lada['new_nom_commercial']= df_lada['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_lada[df_lada['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_lada[(df_lada['nb']>1000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_lada.loc[df_lada['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_lada

def ASTON_MARTIN(df_to_clean):
    df_AM=df_to_clean[df_to_clean['Equiv']=='ASTON MARTIN'].copy()
    df_AM['new_nom_commercial']=df_AM['nom_commercial']
    df_AM.loc[df_AM['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_AM[df_AM['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_AM[df_AM['nb'].astype(str).str.isnumeric()==False]['nb']
    df_AM.loc[df_AM['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['VANTAGE','VANQUISH','DB']

    df_AM['new_nom_commercial']= df_AM['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_AM[df_AM['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_AM[(df_AM['nb']>1000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_AM.loc[df_AM['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_AM)

def ROLLS_ROYCE(df_to_clean):
    df_RR=df_to_clean[df_to_clean['Equiv']=='ROLLS ROYCE'].copy()
    df_RR['new_nom_commercial']=df_RR['nom_commercial']
    df_RR.loc[df_RR['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_RR[df_RR['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_RR[df_RR['nb'].astype(str).str.isnumeric()==False]['nb']
    df_RR.loc[df_RR['nb'].astype(str).str.isnumeric()==False,'nb']=1
    df_RR.loc[df_RR['new_nom_commercial']=='LADA 4 X 4','new_nom_commercial']='LADA 4X4'
    L_modeles=['PHANTOM','SILVER']
    df_RR['new_nom_commercial']= df_RR['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_RR[df_RR['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_RR[(df_RR['nb']>100)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_RR.loc[df_RR['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_RR

def BUICK(df_to_clean):
    df_buick=df_to_clean[df_to_clean['Equiv']=='BUICK'].copy()
    df_buick['new_nom_commercial']=df_buick['nom_commercial']
    df_buick.loc[df_buick['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_buick[df_buick['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_buick[df_buick['nb'].astype(str).str.isnumeric()==False]['nb']
    df_buick.loc[df_buick['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['PHANTOM','SILVER']
    df_buick['new_nom_commercial']= df_buick['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_buick[df_buick['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_buick[(df_buick['nb']>100)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_buick.loc[df_buick['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_buick

def MATRA(df_to_clean):
    df_matra=df_to_clean[df_to_clean['Equiv']=='MATRA'].copy()
    df_matra['new_nom_commercial']=df_matra['nom_commercial']
    df_matra.loc[df_matra['new_nom_commercial']=='E_MO XP','new_nom_commercial']='E-MO'
    df_matra.loc[df_matra['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_matra[df_matra['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_matra[df_matra['nb'].astype(str).str.isnumeric()==False]['nb']
    df_matra.loc[df_matra['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['BAGHEERA','MURENA']
    df_matra['new_nom_commercial']= df_matra['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_matra[df_matra['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_matra[(df_matra['nb']>200)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_matra.loc[df_matra['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_matra

def DATSUN(df_to_clean):
    df_dat=df_to_clean[df_to_clean['Equiv']=='DATSUN'].copy()
    df_dat['new_nom_commercial']=df_dat['nom_commercial']
    df_dat.loc[df_dat['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_dat[df_dat['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_dat[df_dat['nb'].astype(str).str.isnumeric()==False]['nb']
    df_dat.loc[df_dat['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['PICK-UP']
    df_dat['new_nom_commercial']= df_dat['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_dat[df_dat['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_dat[(df_dat['nb']>50)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_dat.loc[df_dat['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_dat

def INFINITY(df_to_clean):
    df_inf=df_to_clean[df_to_clean['Equiv']=='INFINITY'].copy()
    df_inf['new_nom_commercial']=df_inf['nom_commercial']
    df_inf.loc[df_inf['new_nom_commercial']=='E_MO XP','new_nom_commercial']='E-MO'
    df_inf.loc[df_inf['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_inf[df_inf['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_inf[df_inf['nb'].astype(str).str.isnumeric()==False]['nb']
    df_inf.loc[df_inf['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['PICK-UP']
    df_inf['new_nom_commercial']= df_inf['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_inf[df_inf['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_inf[(df_inf['nb']>500)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_inf.loc[df_inf['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_inf

def DAIHATSU(df_to_clean):
    df_dai=df_to_clean[df_to_clean['Equiv']=='DAIHATSU'].copy()
    df_dai['new_nom_commercial']=df_dai['nom_commercial']
    df_dai.loc[df_dai['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_dai[df_dai['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_dai[df_dai['nb'].astype(str).str.isnumeric()==False]['nb']
    df_dai.loc[df_dai['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['PICK-UP']
    df_dai['new_nom_commercial']= df_dai['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_dai[df_dai['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_dai[(df_dai['nb']>1000)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_dai.loc[df_dai['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return(df_dai)

def HUMMER(df_to_clean):
    df_hum=df_to_clean[df_to_clean['Equiv']=='HUMMER'].copy()
    df_hum['new_nom_commercial']=df_hum['nom_commercial']
    df_hum.loc[df_hum['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_hum[df_hum['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_hum[df_hum['nb'].astype(str).str.isnumeric()==False]['nb']
    df_hum.loc[df_hum['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['H3','H2']
    df_hum['new_nom_commercial']= df_hum['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_hum[df_hum['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_hum[(df_hum['nb']>100)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_hum.loc[df_hum['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    df_hum_annexe=df_hum[['new_nom_commercial','nb']]
    df_hum_annexe=df_hum_annexe.groupby(['new_nom_commercial']).agg({'nb':'sum'}).reset_index()
    df_hum_annexe[df_hum_annexe['nb']>0].sort_values('nb',ascending=False).head(20)
    return(df_hum_annexe)

def DAEWOO(df_to_clean):
    df_dae=df_to_clean[df_to_clean['Equiv']=='DAEWOO'].copy()
    df_dae['new_nom_commercial']=df_dae['nom_commercial']
    df_dae.loc[df_dae['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_dae[df_dae['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_dae[df_dae['nb'].astype(str).str.isnumeric()==False]['nb']
    df_dae.loc[df_dae['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['H3','H2']
    df_dae['new_nom_commercial']= df_dae['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_dae[df_dae['nb']>0].sort_values('nb',ascending=False).head(20)
    List_final_name=df_dae[(df_dae['nb']>900)].sort_values('nb',ascending=False)['new_nom_commercial'].to_list()
    df_dae.loc[df_dae['new_nom_commercial'].isin(List_final_name)==False,'new_nom_commercial']='DIVERS'
    return df_dae

def BUGATTI(df_to_clean):
    df_bugatti=df_to_clean[df_to_clean['Equiv']=='BUGATTI'].copy()
    df_bugatti['new_nom_commercial']=df_bugatti['nom_commercial']
    df_bugatti.loc[df_bugatti['nb'].astype(str).str.isnumeric()==False,'new_nom_commercial']= df_bugatti[df_bugatti['nb'].astype(str).str.isnumeric()==False]['new_nom_commercial'].astype(str)+df_bugatti[df_bugatti['nb'].astype(str).str.isnumeric()==False]['nb']
    df_bugatti.loc[df_bugatti['nb'].astype(str).str.isnumeric()==False,'nb']=1
    L_modeles=['H3','H2']
    df_bugatti['new_nom_commercial']= df_bugatti['new_nom_commercial'].apply(detec_name,args=(L_modeles,))
    df_bugatti_annexe=df_bugatti[['new_nom_commercial','nb']]
    df_bugatti_annexe=df_bugatti_annexe.groupby(['new_nom_commercial']).agg({'nb':'sum'}).reset_index()
    df_bugatti_annexe[df_bugatti_annexe['nb']>0].sort_values('nb',ascending=False)['nb'].sum()
    return(df_bugatti_annexe)

def little_brand(df_to_clean):
    df_martin=df_to_clean[df_to_clean['Equiv']=='MARTIN'].copy()
    df_martin['new_nom_commercial']=df_martin['nom_commercial']
    df_martin['new_nom_commercial']='DIVERS'

    df_ds=df_to_clean[df_to_clean['Equiv']=='DS'].copy()
    df_ds['new_nom_commercial']=df_ds['nom_commercial']
    df_ds['new_nom_commercial']='DIVERS'

    df_rangeR=df_to_clean[df_to_clean['Equiv']=='RANGE ROVER'].copy()
    df_rangeR['new_nom_commercial']=df_rangeR['nom_commercial']
    df_rangeR['new_nom_commercial']='DIVERS'

    df_general=df_to_clean[df_to_clean['Equiv']=='GENERAL MOTORS'].copy()
    df_general['new_nom_commercial']=df_general['nom_commercial']
    df_general['new_nom_commercial']='DIVERS'

    df_mustange=df_to_clean[df_to_clean['Equiv']=='MUSTANG'].copy()
    df_mustange['new_nom_commercial']=df_mustange['nom_commercial']
    df_mustange['new_nom_commercial']='DIVERS'

    df_mclaren=df_to_clean[df_to_clean['Equiv']=='MC LAREN'].copy()
    df_mclaren['new_nom_commercial']=df_mclaren['nom_commercial']
    df_mclaren['new_nom_commercial']='DIVERS'

    df_tata=df_to_clean[df_to_clean['Equiv']=='TATA'].copy()
    df_tata['new_nom_commercial']=df_tata['nom_commercial']
    df_tata['new_nom_commercial']='DIVERS'

    df_TRABANT=df_to_clean[df_to_clean['Equiv']=='TRABANT'].copy()
    df_TRABANT['new_nom_commercial']=df_TRABANT['nom_commercial']
    df_TRABANT['new_nom_commercial']='DIVERS'

    df_gm=df_to_clean[df_to_clean['Equiv']=='GM'].copy()
    df_gm['new_nom_commercial']=df_gm['nom_commercial']
    df_gm['new_nom_commercial']='DIVERS'

    df_bwm=df_to_clean[df_to_clean['Equiv']=='BWM'].copy()
    df_bwm['new_nom_commercial']=df_bwm['nom_commercial']
    df_bwm['new_nom_commercial']='DIVERS'

    df_dax=df_to_clean[df_to_clean['Equiv']=='DAX'].copy()
    df_dax['new_nom_commercial']=df_dax['nom_commercial']
    df_dax['new_nom_commercial']='DIVERS'

    df_bmc=df_to_clean[df_to_clean['Equiv']=='BMC'].copy()
    df_bmc['new_nom_commercial']=df_bmc['nom_commercial']
    df_bmc['new_nom_commercial']='DIVERS'

    df_tesla=df_to_clean[df_to_clean['Equiv']=='TESLA'].copy()
    df_tesla['new_nom_commercial']=df_tesla['nom_commercial']
    df_tesla['new_nom_commercial']='DIVERS'

    df_golf=df_to_clean[df_to_clean['Equiv']=='GOLF'].copy()
    df_golf['new_nom_commercial']=df_golf['nom_commercial']
    df_golf['new_nom_commercial']='DIVERS'

    df_abarth=df_to_clean[df_to_clean['Equiv']=='ABARTH'].copy()
    df_abarth['new_nom_commercial']=df_abarth['nom_commercial']
    df_abarth['new_nom_commercial']='DIVERS'

    df_sub=df_to_clean[df_to_clean['Equiv']=='SUBARU'].copy()
    df_sub['new_nom_commercial']=df_sub['nom_commercial']
    df_sub['new_nom_commercial']='DIVERS'

    df_ssang=df_to_clean[df_to_clean['Equiv']=='SSANGYONG'].copy()
    df_ssang['new_nom_commercial']=df_ssang['nom_commercial']
    df_ssang['new_nom_commercial']='DIVERS'

    df_mehari=df_to_clean[df_to_clean['Equiv']=='MEHARI'].copy()
    df_mehari['new_nom_commercial']=df_mehari['nom_commercial']
    df_mehari['new_nom_commercial']='DIVERS'

    df_saab=df_to_clean[df_to_clean['Equiv']=='SAAD'].copy()
    df_saab['new_nom_commercial']=df_saab['nom_commercial']
    df_saab['new_nom_commercial']='DIVERS'
    l=[df_martin,df_ds,df_rangeR,df_general,df_mustange,df_mclaren,df_tata,df_TRABANT,df_bwm,df_gm,df_dax,df_bmc,
       df_tesla,df_golf,df_abarth,df_sub,df_ssang,df_mehari,df_saab]
    df_final = df_martin
    for i in l[1:]:
        df_final=pd.concat([df_final,i])
    return(df_final)

# # Final base


l=[FIAT(df_to_clean), RENAULT(df_to_clean), PEUGEOT(df_to_clean), CITROEN(df_to_clean), MERCEDES(df_to_clean),
   VOLKSWAGEN(df_to_clean), NISSAN(df_to_clean), MBK(df_to_clean), OPEL(df_to_clean), HONDA(df_to_clean),
   ALFA_ROMEO(df_to_clean), FORD(df_to_clean), BMW(df_to_clean), AUDI(df_to_clean), LANCIA(df_to_clean),
   TOYOTA(df_to_clean), SEAT(df_to_clean), VOLVO(df_to_clean), TRIGANO(df_to_clean), PORSCHE(df_to_clean),
   SKODA(df_to_clean), TRIUMPH(df_to_clean), MITSUBISHI(df_to_clean), ROVER(df_to_clean), CHEVROLET(df_to_clean),
   JAGUAR(df_to_clean), DODGE(df_to_clean), MAZDA(df_to_clean), LAND_ROVER(df_to_clean), VESPA(df_to_clean),
   FERRARI(df_to_clean), MINI(df_to_clean), LAMBORGHINI(df_to_clean), ISUZU(df_to_clean), HYUNDAI(df_to_clean),
   DACIA(df_to_clean), CHRYSLER(df_to_clean), AUSTIN(df_to_clean), SIMCA(df_to_clean), MG(df_to_clean), JEEP(df_to_clean),
   KIA(df_to_clean), DAIMLER(df_to_clean), LOTUS(df_to_clean), GMC(df_to_clean), MASERATI(df_to_clean),
   SMART(df_to_clean), CADILLAC(df_to_clean), LEXUS(df_to_clean), BENTLEY(df_to_clean), LADA(df_to_clean),
   ASTON_MARTIN(df_to_clean), ROLLS_ROYCE(df_to_clean), BUICK(df_to_clean), MATRA(df_to_clean), DATSUN(df_to_clean),
   INFINITY(df_to_clean), DAIHATSU(df_to_clean), HUMMER(df_to_clean), DAEWOO(df_to_clean), BUGATTI(df_to_clean),
   little_brand(df_to_clean)]

df_final = l[0]
for i in l[1:]:
    df_final = pd.concat([df_final, i])

df_final['nom_commercial_id']=df_final['Equiv']+'_'+df_final['new_nom_commercial']
df_final = df_final[['Equiv','nom_commercial','nom_commercial_id']]
df_final['nom_commercial_id'].nunique()

#df_final.to_csv('denom_commerciale_simplified.xslx')


