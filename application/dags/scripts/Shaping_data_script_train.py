#! /usr/bin/env python3


'''
Ce script permet de réaliser l'encodage et la normalisation des bases d'entrainement ou de test ainsi que la préparation
 de la variable objectif y correspondante.
 Il faut exécuter le programmme une fois pour chaque base pour ne pas saturer la mémoire. 
 On fournit en entrée la base à encoder et en sortie nous récupérons la base contenant les variables (X) et la variable
 ciblée.
'''


def main():
    import pandas as pd
    import pickle
    import numpy as np
    from sklearn.preprocessing import MinMaxScaler
    from sklearn.preprocessing import OneHotEncoder
    import mysql.connector as mysql
    from mysql.connector import Error
    from sqlalchemy import create_engine
    import os

    db_connection = create_engine(
        "mysql+pymysql://{user}:{pw}@{host}/{db}".format(host=os.getenv("MYSQL_HOSTNAME"), db=os.getenv("MYSQL_DBNAME"),
                                                         user=os.getenv("MYSQL_USER"),
                                                         pw=os.getenv("MYSQL_ROOT_PASSWORD")))
    # shaping_input_filename = 'vehicule_data_train_grouped.csv'
    shaping_output_X_filename = '/tmp/X_full_train.pkl'
    shaping_output_y_filename = '/tmp/y_train_encoded_full.pkl'
    '''verbose = args.verbose'''

    df_to_encode = pd.read_sql('SELECT * FROM final_train', con=db_connection)

    # file_name = 'vehicule_data_' + file_type + '_grouped.csv'
    '''df_to_encode = pd.read_csv(shaping_input_filename,
                               dtype={'age_vehicule': 'float16', 'age_certificat': 'float16', 'dept': 'float16',
                                      'nom_commercial': 'category', 'marque_id': 'float16', 'code_energie': 'category',
                                      'nb_titulaires': 'int16',
                                      'is_apte_a_circuler': 'bool', 'pers_locataire': 'bool', 'nb_sinistres': 'int8',
                                      'critair': 'category',
                                      'ref_carrosserie_id': 'category', 'CTEC_PUISS_CV': 'float16',
                                      'nb_observations': 'int16',
                                      'is_observed': 'bool'})'''
    print('datafile loaded')
    df_to_encode.set_index('ref_vehc_id', inplace=True)

    print(df_to_encode.shape)
    # df_to_encode['is_apte_a_circuler'] = df_to_encode['is_apte_a_circuler'].map({'True': 1, 'False': 0}).fillna(
    #    0).astype('int8')
    # df_to_encode['pers_locataire'] = df_to_encode['pers_locataire'].map({'True': 1, 'False': 0}).fillna(0).astype(
    #    'int8')

    df_to_encode['age_certificat'].fillna(df_to_encode['age_vehicule'], inplace=True)

    '''with open("dept_dict.pkl", "rb") as fh:
        dept_dict = pickle.load(fh)
    df_to_encode['vehic_per_hab'] = df_to_encode['dept'].map(dept_dict)
    del dept_dict'''

    '''df_nomcom = pd.read_csv('Clean_commercial_names.csv')
    dico_nomcom = dict(zip(df_nomcom['nom_commercial'], df_nomcom['nom_commercial_2']))
    dico_nomcom['DIVERS_DIVERS'] = 'DIVERS_DIVERS'
    df_to_encode['nom_com'] = df_to_encode['nom_commercial'].map(dico_nomcom).astype('category')
    del df_nomcom'''

    df_to_encode.dropna(inplace=True)
    df_to_encode.reset_index(inplace=True)

    X = df_to_encode
    y = df_to_encode[['nb_observations']]
    del df_to_encode

    a0 = 0
    a1 = 1  # y_train_new[y_train_new['nb_observations']>0]['nb_observations'].quantile(0.5)
    a2 = 2  # y_train_new[y_train_new['nb_observations']>0]['nb_observations'].quantile(0.95)
    a3 = 3  # y_train_new[y_train_new['nb_observations']>0]['nb_observations'].quantile(0.99)
    dico_obs = {a0: 'unobserved', a1: 'one_time', a2: 'two_times', a3: 'tree_times'}

    y['nb_observations'] = y['nb_observations'].map(dico_obs)
    y['nb_observations'].fillna('very_watched', inplace=True)

    columns_num = ['age_vehicule', 'age_certificat', 'nb_titulaires', 'nb_sinistres', 'CTEC_PUISS_CV',
                   'nb_observations',
                   'vehic_per_hab']
    std_scaler_train = MinMaxScaler().fit(X[columns_num])
    Num_scaled_train = std_scaler_train.transform(X[columns_num])
    Trained_num = pd.DataFrame(Num_scaled_train, columns=columns_num, dtype='float16')

    columns_cat = ['code_energie', 'is_apte_a_circuler', 'pers_locataire', 'critair', 'ref_carrosserie_id', 'nom_com']
    X_encoded = pd.concat([X[columns_cat], Trained_num], axis=1)

    OH_encoder = OneHotEncoder(handle_unknown='ignore', sparse=False, dtype='uint8')
    train_encoder = OH_encoder.fit_transform(y)
    columns_name_train = OH_encoder.get_feature_names_out()
    y_train_encoded = pd.DataFrame(train_encoder, columns=columns_name_train)

    with open(shaping_output_y_filename, "wb") as fh:
        pickle.dump(y_train_encoded, fh)
    del y_train_encoded
    del X

    l_critair = list(map(lambda ls: 'critair_' + ls, X_encoded['critair'].unique().tolist()))
    l_nom_com = list(map(lambda ls: 'nom_com_' + ls, X_encoded['nom_com'].unique().tolist()))
    l_code_energ = list(map(lambda ls: 'code_energie_' + str(ls), X_encoded['code_energie'].unique().tolist()))
    l_ref_car = list(map(lambda ls: 'ref_carrosserie_id_' + str(ls), X_encoded['ref_carrosserie_id'].unique().tolist()))

    big_l = l_critair + l_nom_com + l_code_energ + l_ref_car
    len_col = len(big_l)
    nb_row = X_encoded.shape[0]
    zeros_tables = np.zeros((nb_row, len_col), dtype=np.int8)
    zero_df = pd.DataFrame(zeros_tables, columns=big_l, dtype='int8')
    print(zero_df.dtypes)
    print(X_encoded.dtypes)
    X_encoded = pd.concat([X_encoded, zero_df], axis=1)
    print(X_encoded.dtypes)

    for i in X_encoded.index:
        X_encoded.at[i, 'critair_' + str(X_encoded.at[i, 'critair'])] = 1
        X_encoded.at[i, 'nom_com_' + str(X_encoded.at[i, 'nom_com'])] = 1
        X_encoded.at[i, 'code_energie_' + str(X_encoded.at[i, 'code_energie'])] = 1
        X_encoded.at[i, 'ref_carrosserie_id_' + str(X_encoded.at[i, 'ref_carrosserie_id'])] = 1

    # X_encoded = X_encoded.parrallel_apply(hot_function, axis=1)
    X_encoded.drop(['critair', 'nom_com', 'ref_carrosserie_id', 'code_energie'], inplace=True, axis=1)

    with open(shaping_output_X_filename, "wb") as fh:
        pickle.dump(X_encoded, fh)


if __name__ == "__main__":
    main()
