#! /usr/bin/env python3
from Rename_Nom_Com import *
import pandas as pd
import argparse


'''
Ce script permet de nettoyer et ne garder que les noms commerciaux principaux par marques prinicpales afin 
d'éviter un trop grand nombre de modélités pour la variable 'nom commercial' tout en apportant un paramètre en plus 
pour les véhicules récurrents.
On fournit en entrée un fichier excel contenant la totalité des noms commerciaux brutes et nous récupérons en sortie une 
table d'équivalence entre les noms commerciaux originels et la version simplifiée et épurée.
'''

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--denom_input',
                        default='denom_commerciale.xlsx',
                        help='input file for uncleaned commercial names',
                        required=True)
    parser.add_argument('-o', '--denom_output',
                        default='Clean_commercial_names.csv',
                        help='output file for cleaned commercial names',
                        required=True)
    '''parser.add_argument('-v', '--verbose',
                        default=True,
                        help='more verbose output on stdout')'''

    args = parser.parse_args()
    denom_commerciale_input_filename = args.denom_input
    denom_commerciale_output_filename = args.denom_output
    '''verbose = args.verbose'''

    # Install openpyxl if not done
    df_to_clean = pd.read_excel(denom_commerciale_input_filename, dtype={'Equiv':str, 'nom_commercial':str, 'nb':int})

    threshold = 1
    while df_to_clean['Equiv'].value_counts().head(threshold).sum() / df_to_clean['Equiv'].value_counts().sum() <= 0.85:
        threshold += 1

    List_major = df_to_clean['Equiv'].value_counts().head(threshold).index.tolist()
    df_modified = eval(List_major[0] + '(df_to_clean)')

    for i in List_major[1:]:
        a = i + '(df_to_clean)'
        a = a.replace(' ', '_')
        df_modified = pd.concat([df_modified, eval(a)])
    df_modified['new_nom_commercial'] = df_modified['Equiv'] + '_' + df_modified['new_nom_commercial']
    df_modified['nom_commercial'] = df_modified['Equiv'] + '_' + df_modified['nom_commercial'].astype(str)

    df_to_clean['nom_commercial'] = df_to_clean['Equiv'] + '_' + df_to_clean['nom_commercial'].astype(str)

    dico_brand = dict(zip(df_modified['nom_commercial'], df_modified['new_nom_commercial']))

    df_to_clean['nom_commercial_1'] = df_to_clean['nom_commercial'].map(dico_brand)
    df_to_clean['nom_commercial_1'].fillna('DIVERS_DIVERS', inplace=True)

    Luxe = ['PORSCHE', 'ROVER', 'JAGUAR', 'LAND ROVER', 'FERRARI', 'LAMBORGHINI', 'MASERATI', 'BENTLEY', 'ASTON MARTIN',
            'ROLLS ROYCE', 'BUGATTI', 'MARTIN', 'RANGE ROVER', 'MC LAREN', 'TESLA']

    df_to_clean['nom_commercial_2'] = df_to_clean['nom_commercial_1']

    for i in Luxe:
        df_to_clean.loc[df_to_clean['nom_commercial'].str.startswith(i), 'nom_commercial_2'] = 'LUXE_DIVERS'

    if verbose:
        print(df_to_clean['nom_commercial_1'].nunique(), df_to_clean['nom_commercial_2'].nunique())

    df_to_clean.to_csv('~/Desktop/vehicle_analysis/data/mysql/'+ denom_commerciale_output_filename, sep=';')
