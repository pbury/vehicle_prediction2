USE histovec;

DROP TABLE IF EXISTS table_raw;

CREATE TABLE table_raw (dos_id BIGINT NOT NULL, CTEC_PUISS_CV VARCHAR(64) NULL, adr_code_postal_tit VARCHAR(24) NULL,
 marque VARCHAR(64) NULL,
    nom_commercial VARCHAR(128) NULL, date_premiere_immat VARCHAR(64) NULL, date_emission_CI VARCHAR(64) NULL,
    nb_titulaires INT NULL, is_apte_a_circuler VARCHAR(24) NULL, pers_locataire VARCHAR(24) NULL, CTEC_RLIB_ENERGIE VARCHAR(16) NULL,
    nb_sinistres INT NULL, critair VARCHAR(24) NULL, CTEC_RLIB_CARROSSERIE_NAT VARCHAR(64) NULL, utac_encrypted_vin VARCHAR(64) NOT NULL,
    utac_encrypted_immat VARCHAR(64) NOT NULL, CTEC_RLIB_CATEGORIE VARCHAR(24) NULL, CTEC_RLIB_GENRE  VARCHAR(24) NULL);

LOAD DATA INFILE '/input_data/table_initiale_filled.csv' into table table_raw fields terminated by ',' ignore 1 LINES;
