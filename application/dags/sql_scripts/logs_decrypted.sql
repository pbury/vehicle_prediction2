DROP TABLE IF EXISTS source_logs;
DROP TABLE IF EXISTS logs_train;
DROP TABLE IF EXISTS logs_test;
DROP TABLE IF EXISTS counting_test;
DROP TABLE IF EXISTS counting_train;

CREATE TABLE source_logs (datation VARCHAR(64) NULL, utac_encrypted_immat VARCHAR(64) NOT NULL,
utac_encrypted_vin VARCHAR(64) NOT NULL);

LOAD DATA INFILE '/input_data/source_logs_sql.csv' into table source_logs fields terminated by ',' ignore 1 LINES;

UPDATE source_logs
SET datation = LEFT(datation, 8);

ALTER TABLE source_logs
MODIFY COLUMN datation date NULL;

ALTER TABLE source_logs
ADD COLUMN unique_id VARCHAR(64);

CREATE INDEX idx_source_logs_unique_id ON source_logs (unique_id);

UPDATE source_logs
SET unique_id = CONCAT(utac_encrypted_vin,'_',utac_encrypted_immat);

CREATE TABLE logs_train LIKE source_logs;
CREATE TABLE logs_test LIKE source_logs;

-- TODO Pour phase de test remplacer DATE_FORMAT(...) par '2022-03-01'
INSERT INTO logs_train SELECT * FROM source_logs  WHERE datation < DATE_FORMAT(NOW() ,'%Y-%m-01');
INSERT INTO logs_test SELECT * FROM source_logs;

create table counting_test (unique_id varchar(128), counts int);
create table counting_train (unique_id varchar(128), counts int);

Insert counting_test
SELECT unique_id, COUNT(unique_id) AS count
FROM logs_test
GROUP BY unique_id;

Insert counting_train
SELECT unique_id, COUNT(unique_id) AS count
FROM logs_train
GROUP BY unique_id;

