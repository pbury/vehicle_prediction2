DROP TABLE IF EXISTS table_train;

CREATE TABLE table_train LIKE table_intermediaire;
INSERT INTO table_train SELECT * FROM table_intermediaire;

ALTER TABLE table_train
ADD COLUMN (datation varchar (64), nb_observations int);

CREATE INDEX idx_table_train_unique_id ON table_train (unique_id);

UPDATE table_train tta
JOIN counting_train cta
on tta.unique_id = cta.unique_id
SET tta.nb_observations = cta.counts;

CREATE INDEX idx_table_train_datation ON table_train (datation);

UPDATE table_train
SET table_train.datation = (SELECT MAX(datation)
               FROM logs_train
               WHERE logs_train.unique_id = table_train.unique_id);

ALTER TABLE table_train
ADD COLUMN is_observed int(1);

CREATE INDEX idx_table_train_is_observed ON table_train (is_observed);
CREATE INDEX idx_table_train_nb_observations ON table_train (nb_observations);

UPDATE table_train
SET is_observed = CASE
   WHEN nb_observations > 0 THEN 1
   ELSE 0
END;

UPDATE table_train
SET nb_observations = CASE
   WHEN nb_observations is null THEN 0
   ELSE nb_observations
END;
