DROP TABLE IF EXISTS ref_carrosserie;

CREATE TABLE ref_carrosserie ( ref_carrosserie_id INT NOT NULL AUTO_INCREMENT, ref_carrosserie varchar(200), nb int,
    PRIMARY KEY (ref_carrosserie_id)
                            );

INSERT INTO ref_carrosserie (ref_carrosserie,nb ) (SELECT CTEC_RLIB_CARROSSERIE_NAT, count(1) as nb
                                                              from dedoub_table
                                                              group by CTEC_RLIB_CARROSSERIE_NAT order by nb desc);


