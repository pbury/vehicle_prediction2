DROP TABLE If EXISTS final_test;
DROP TABLE IF EXISTS equiv_dos_id_test;

CREATE TABLE final_test
SELECT CTEC_PUISS_CV, dept, nom_com, nb_titulaires, is_apte_a_circuler, pers_locataire, code_energie, nb_sinistres, critair,
       ref_carrosserie_id, age_vehicule, age_certificat, vehic_per_hab, MAX(datation) as datation, sum(nb_observations) as
    nb_observations
FROM table_test
GROUP BY CTEC_PUISS_CV, dept, nom_com, nb_titulaires, is_apte_a_circuler, pers_locataire, code_energie, nb_sinistres,
         critair, ref_carrosserie_id, age_vehicule, age_certificat, vehic_per_hab;

ALTER TABLE final_test
ADD COLUMN ref_vehc_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

CREATE INDEX idx_table_test_CTEC_puiss_CV ON table_test (CTEC_puiss_CV);
CREATE INDEX idx_table_test_dept ON table_test (dept);
CREATE INDEX idx_table_test_nom_com ON table_test (nom_com);
CREATE INDEX idx_table_test_nb_titulaires ON table_test (nb_titulaires);
CREATE INDEX idx_table_test_is_apte_a_circuler ON table_test (is_apte_a_circuler);
CREATE INDEX idx_table_test_pers_locataire ON table_test (pers_locataire);
CREATE INDEX idx_table_test_code_energie ON table_test (code_energie);
CREATE INDEX idx_table_test_nb_sinistres ON table_test (nb_sinistres);
CREATE INDEX idx_table_test_critair ON table_test (critair);
CREATE INDEX idx_table_test_ref_carrosserie_id ON table_test (ref_carrosserie_id);
CREATE INDEX idx_table_test_age_vehicule ON table_test (age_vehicule);
CREATE INDEX idx_table_test_age_certificat ON table_test (age_certificat);
CREATE INDEX idx_table_test_vehic_per_hab ON table_test (vehic_per_hab);

CREATE INDEX idx_final_test_CTEC_puiss_CV ON final_test (CTEC_puiss_CV);
CREATE INDEX idx_final_test_dept ON final_test (dept);
CREATE INDEX idx_final_test_nom_com ON final_test (nom_com);
CREATE INDEX idx_final_test_nb_titulaires ON table_test (nb_titulaires);
CREATE INDEX idx_final_test_is_apte_a_circuler ON final_test (is_apte_a_circuler);
CREATE INDEX idx_final_test_pers_locataire ON final_test (pers_locataire);
CREATE INDEX idx_final_test_code_energie ON final_test (code_energie);
CREATE INDEX idx_final_test_nb_sinistres ON final_test (nb_sinistres);
CREATE INDEX idx_final_test_critair ON final_test (critair);
CREATE INDEX idx_final_test_ref_carrosserie_id ON final_test (ref_carrosserie_id);
CREATE INDEX idx_final_test_age_vehicule ON final_test (age_vehicule);
CREATE INDEX idx_final_test_age_certificat ON final_test (age_certificat);
CREATE INDEX idx_final_test_vehic_per_hab ON final_test (vehic_per_hab);

CREATE TABLE equiv_dos_id_test
SELECT tt.dos_id, ft.ref_vehc_id
FROM table_test tt
JOIN final_test ft on tt.CTEC_puiss_CV = ft.CTEC_PUISS_CV and tt.dept=ft.dept and tt.nom_com = ft.nom_com and
    tt.nb_titulaires=ft.nb_titulaires and tt.is_apte_a_circuler=ft.is_apte_a_circuler and tt.pers_locataire=ft.pers_locataire
    and tt.code_energie=ft.code_energie and tt.nb_sinistres=ft.nb_sinistres and tt.critair=ft.critair and
    tt.ref_carrosserie_id=ft.ref_carrosserie_id and tt.age_vehicule=ft.age_vehicule and tt.age_certificat=ft.age_certificat
    and tt.vehic_per_hab=ft.vehic_per_hab;



