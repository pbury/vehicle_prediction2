DROP TABLE IF EXISTS table_train;
DROP TABLE IF EXISTS table_test;

CREATE TABLE table_train LIKE table_intermediaire;
INSERT INTO table_train SELECT * FROM table_intermediaire;
CREATE TABLE table_test LIKE table_intermediaire;
INSERT INTO table_test SELECT * FROM table_intermediaire;

ALTER TABLE table_test
ADD COLUMN (datation varchar (64), nb_observations int);
ALTER TABLE table_train
ADD COLUMN (datation varchar (64), nb_observations int);

UPDATE table_test tt
JOIN counting_test ct
on tt.unique_id = ct.unique_id
SET tt.nb_observations = ct.counts;

UPDATE table_train tta
JOIN counting_train cta
on tta.unique_id = cta.unique_id
SET tta.nb_observations = cta.counts;

UPDATE table_test
SET table_test.datation = (SELECT MAX(datation)
               FROM logs_test
               WHERE logs_test.unique_id = table_test.unique_id);

UPDATE table_train
SET table_train.datation = (SELECT MAX(datation)
               FROM logs_train
               WHERE logs_train.unique_id = table_train.unique_id);

ALTER TABLE table_test
ADD COLUMN is_observed int(1);
ALTER TABLE table_train
ADD COLUMN is_observed int(1);

UPDATE table_test
SET is_observed = CASE
   WHEN MONTH(datation)=3 AND nb_observations >0 THEN 1
   ELSE 0
END;

UPDATE table_test
SET nb_observations = CASE
   WHEN nb_observations is null THEN 0
   ELSE nb_observations
END;

UPDATE table_train
SET is_observed = CASE
   WHEN nb_observations > 0 THEN 1
   ELSE 0
END;

UPDATE table_train
SET nb_observations = CASE
   WHEN nb_observations is null THEN 0
   ELSE nb_observations
END;

'''
SELECT *
FROM table_train'''
#INTO OUTFIlE '/var/lib/mysql-files/vehicule_data_train_grouped.csv';
'''SELECT *
FROM table_test '''
#INTO OUTFIlE '/var/lib/mysql-files/vehicule_data_test_grouped.csv';
