DROP TABLE IF EXISTS table_intermediaire;

ALTER TABLE dedoub_table
ADD COLUMN (ref_carrosserie_id INT NULL, nom_com VARCHAR(128) NULL, Equiv VARCHAR(64) NULL);

CREATE INDEX idx_dedoub_equiv ON dedoub_table (Equiv);
CREATE INDEX idx_dedoub_nom_com ON dedoub_table (nom_com);
CREATE INDEX idx_dedoub_ref_car_id ON dedoub_table (ref_carrosserie_id);

UPDATE dedoub_table dt
JOIN ref_marque rm on dt.marque = rm.marque
SET dt.Equiv = rm.Equiv;

UPDATE dedoub_table dt
JOIN denom_commerciale dc on dt.nom_commercial = dc.nom_commercial AND dt.Equiv= dc.Equiv
SET dt.nom_com = dc.nom_commercial_2;

UPDATE dedoub_table dt
JOIN ref_carrosserie rc on dt.CTEC_RLIB_CARROSSERIE_NAT = rc.ref_carrosserie
SET dt.ref_carrosserie_id = rc.ref_carrosserie_id;

UPDATE dedoub_table SET Equiv = CASE
    WHEN Equiv is null then 'DIVERS' else Equiv
END;

UPDATE dedoub_table SET nom_com = CASE
    WHEN nom_com is null then CONCAT(Equiv,'_','DIVERS')
else dedoub_table.nom_com
END;

CREATE TABLE table_intermediaire
SELECT dos_id, CTEC_PUISS_CV, dept, nom_com, date_premiere_immat ,
date_emission_CI , nb_titulaires , is_apte_a_circuler , pers_locataire,
code_energie, nb_sinistres, critair, ref_carrosserie_id,
utac_encrypted_vin , utac_encrypted_immat
    FROM dedoub_table;

CREATE INDEX idx_table_int_dept ON table_intermediaire (dept);

UPDATE table_intermediaire
SET date_premiere_immat = LEFT(date_premiere_immat,10 ),
date_emission_CI = LEFT(date_emission_CI,10 );

ALTER TABLE table_intermediaire
MODIFY COLUMN date_premiere_immat date NULL ,
MODIFY COLUMN date_emission_CI date NULL;

ALTER TABLE table_intermediaire
ADD COLUMN age_vehicule int, ADD age_certificat int, ADD unique_id VARCHAR(64), ADD vehic_per_hab FLOAT NULL;

CREATE INDEX idx_table_in_vph ON table_intermediaire (vehic_per_hab);

UPDATE table_intermediaire
SET unique_id = CONCAT(utac_encrypted_vin,'_',utac_encrypted_immat),
    age_vehicule = DATEDIFF(CURDATE(), date_premiere_immat),
    age_certificat = DATEDIFF(CURDATE(),date_emission_CI);

CREATE INDEX idx_vph_dept ON vehicule_per_hab (dept);
CREATE INDEX idx_vph_vph ON vehicule_per_hab (vehic_per_hab);

UPDATE table_intermediaire ti
JOIN vehicule_per_hab vph on ti.dept = vph.dept
SET ti.vehic_per_hab = vph.vehic_per_hab;

UPDATE table_intermediaire
SET vehic_per_hab = ( SELECT AVG(vehic_per_hab) FROM vehicule_per_hab TIN)
WHERE table_intermediaire.vehic_per_hab IS NULL;

DROP TABLE IF EXISTS dedoub_table;






