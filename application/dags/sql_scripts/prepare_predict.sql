DROP TABLE IF EXISTS table_test;

CREATE TABLE table_test LIKE table_intermediaire;
INSERT INTO table_test SELECT * FROM table_intermediaire;

ALTER TABLE table_test
ADD COLUMN (datation varchar (64), nb_observations int);

CREATE INDEX idx_table_test_unique_id ON table_test (unique_id);

UPDATE table_test tt
JOIN counting_test ct
on tt.unique_id = ct.unique_id
SET tt.nb_observations = ct.counts;

CREATE INDEX idx_table_test_datation ON table_test (datation);

UPDATE table_test
SET table_test.datation = (SELECT MAX(datation)
               FROM logs_test
               WHERE logs_test.unique_id = table_test.unique_id);

ALTER TABLE table_test
ADD COLUMN is_observed int(1);

CREATE INDEX idx_table_test_is_observed ON table_test (is_observed);
CREATE INDEX idx_table_test_nb_observations ON table_test (nb_observations);

-- TODO Pour phase de test remplacer month(current_date) par 3 et year par 20222
UPDATE table_test
SET is_observed = CASE
   WHEN MONTH(datation)=MONTH(CURRENT_DATE())-1 AND YEAR(datation)=YEAR(CURRENT_DATE()) AND nb_observations >0 THEN 1
   ELSE 0
END;

UPDATE table_test
SET nb_observations = CASE
   WHEN nb_observations is null THEN 0
   ELSE nb_observations
END;

