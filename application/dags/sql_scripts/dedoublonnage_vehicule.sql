DROP TABLE IF EXISTS dedoub_table;
DROP TABLE IF EXISTS dept_amount;

CREATE TABLE dedoub_table
SELECT  dos_id, CTEC_PUISS_CV,dept,marque,nom_commercial,date_premiere_immat,date_emission_CI,
       nb_titulaires,is_apte_a_circuler,pers_locataire,code_energie,nb_sinistres,critair,
       CTEC_RLIB_CARROSSERIE_NAT,utac_encrypted_vin,utac_encrypted_immat, CTEC_RLIB_CATEGORIE, CTEC_RLIB_GENRE
  FROM (
    SELECT *, ROW_NUMBER() OVER (PARTITION BY dos_id ORDER BY dos_id DESC) AS rownumber
      FROM table_raw
      WHERE CTEC_RLIB_CATEGORIE like 'M1' OR CTEC_RLIB_GENRE like 'VP'
    ) unfiltered_query
 WHERE rownumber = 1 AND (CTEC_RLIB_CATEGORIE like 'M1' OR CTEC_RLIB_GENRE like 'VP');

DROP TABLE IF EXISTS table_raw;

CREATE INDEX idx_dedoub_table_marque ON dedoub_table (marque);
CREATE INDEX idx_dedoub_table_nom_com ON dedoub_table (nom_commercial);
CREATE INDEX idx_dedoub_table_carrosserie ON dedoub_table (CTEC_RLIB_CARROSSERIE_NAT);

CREATE TABLE dept_amount
SELECT dept, count(dept) as nb
FROM dedoub_table
GROUP BY dept;

