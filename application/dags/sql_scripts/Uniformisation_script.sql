UPDATE table_raw
SET adr_code_postal_tit = LEFT(adr_code_postal_tit,2 );

UPDATE table_raw
SET adr_code_postal_tit = 99
WHERE adr_code_postal_tit='';

ALTER TABLE table_raw
CHANGE adr_code_postal_tit dept INT NULL;

ALTER TABLE table_raw
CHANGE CTEC_RLIB_ENERGIE code_energie VARCHAR(10) NULL;

UPDATE table_raw SET is_apte_a_circuler = CASE
    WHEN is_apte_a_circuler ='OUI' THEN 1 ELSE 0
END,
    pers_locataire= CASE
    WHEN pers_locataire = 'OUI' THEN 1
    ELSE 0
END,
code_energie =CASE
    WHEN code_energie IN ('GA', 'GE', 'GF', 'GG', 'GH', 'GO', 'GQ', 'PL') THEN 1
    WHEN code_energie IN ('AC', 'EL', 'H2', 'HE', 'HH') THEN 2
    WHEN code_energie IN ('EH', 'ES', 'ET', 'FE', 'FH') THEN 3
    WHEN code_energie IN ('EG', 'EN', 'EP', 'EQ', 'FG', 'FN', 'G2', 'GN', 'GP', 'GZ', 'NH', 'PH') THEN 4
    WHEN code_energie IN ('EE', 'EM', 'ER', 'FL', 'GL', 'GM', 'NE', 'PE') THEN 5
    ELSE 0
END,
critair = CASE
    when critair like 'Electrique' THEN 0
    when critair like '1' THEN 1
    when critair like '2' THEN 2
    when critair like '3' THEN 3
    when critair like '4' THEN 4
    when critair like '5' THEN 5
    when critair like 'NON_CLASSE' THEN 6
    when critair is null THEN 6
END
WHERE CTEC_RLIB_CATEGORIE like 'M1' OR CTEC_RLIB_GENRE like 'VP';



