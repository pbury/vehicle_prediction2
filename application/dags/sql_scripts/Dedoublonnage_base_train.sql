DROP TABLE IF EXISTS final_train;
DROP TABLE IF EXISTS equiv_dos_id_train;

CREATE TABLE final_train
SELECT CTEC_PUISS_CV, dept, nom_com, nb_titulaires, is_apte_a_circuler, pers_locataire, code_energie, nb_sinistres, critair,
       ref_carrosserie_id, age_vehicule, age_certificat, vehic_per_hab, MAX(datation) as datation, cast(sum(nb_observations) as unsigned) as
    nb_observations
FROM table_train
GROUP BY CTEC_PUISS_CV, dept, nom_com, nb_titulaires, is_apte_a_circuler, pers_locataire, code_energie, nb_sinistres,
         critair, ref_carrosserie_id, age_vehicule, age_certificat, vehic_per_hab;

ALTER TABLE final_train
ADD COLUMN ref_vehc_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

CREATE INDEX idx_table_train_CTEC_puiss_CV ON table_train (CTEC_puiss_CV);
CREATE INDEX idx_table_train_dept ON table_train (dept);
CREATE INDEX idx_table_train_nom_com ON table_train (nom_com);
CREATE INDEX idx_table_train_nb_titulaires ON table_train (nb_titulaires);
CREATE INDEX idx_table_train_is_apte_a_circuler ON table_train (is_apte_a_circuler);
CREATE INDEX idx_table_train_pers_locataire ON table_train (pers_locataire);
CREATE INDEX idx_table_train_code_energie ON table_train (code_energie);
CREATE INDEX idx_table_train_nb_sinistres ON table_train (nb_sinistres);
CREATE INDEX idx_table_train_critair ON table_train (critair);
CREATE INDEX idx_table_train_ref_carrosserie_id ON table_train (ref_carrosserie_id);
CREATE INDEX idx_table_train_age_vehicule ON table_train (age_vehicule);
CREATE INDEX idx_table_train_age_certificat ON table_train (age_certificat);
CREATE INDEX idx_table_train_vehic_per_hab ON table_train (vehic_per_hab);

CREATE INDEX idx_final_train_CTEC_puiss_CV ON final_train (CTEC_puiss_CV);
CREATE INDEX idx_final_train_dept ON final_train (dept);
CREATE INDEX idx_final_train_nom_com ON final_train (nom_com);
CREATE INDEX idx_final_train_nb_titulaires ON table_train (nb_titulaires);
CREATE INDEX idx_final_train_is_apte_a_circuler ON final_train (is_apte_a_circuler);
CREATE INDEX idx_final_train_pers_locataire ON final_train (pers_locataire);
CREATE INDEX idx_final_train_code_energie ON final_train (code_energie);
CREATE INDEX idx_final_train_nb_sinistres ON final_train (nb_sinistres);
CREATE INDEX idx_final_train_critair ON final_train (critair);
CREATE INDEX idx_final_train_ref_carrosserie_id ON final_train (ref_carrosserie_id);
CREATE INDEX idx_final_train_age_vehicule ON final_train (age_vehicule);
CREATE INDEX idx_final_train_age_certificat ON final_train (age_certificat);
CREATE INDEX idx_final_train_vehic_per_hab ON final_train (vehic_per_hab);


CREATE TABLE equiv_dos_id_train
SELECT tt.dos_id, ft.ref_vehc_id
FROM table_train tt
JOIN final_train ft on tt.CTEC_puiss_CV = ft.CTEC_PUISS_CV and tt.dept=ft.dept and tt.nom_com = ft.nom_com and
    tt.nb_titulaires=ft.nb_titulaires and tt.is_apte_a_circuler=ft.is_apte_a_circuler and tt.pers_locataire=ft.pers_locataire
    and tt.code_energie=ft.code_energie and tt.nb_sinistres=ft.nb_sinistres and tt.critair=ft.critair and
    tt.ref_carrosserie_id=ft.ref_carrosserie_id and tt.age_vehicule=ft.age_vehicule and tt.age_certificat=ft.age_certificat
    and tt.vehic_per_hab=ft.vehic_per_hab;

