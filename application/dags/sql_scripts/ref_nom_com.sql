DROP TABLE IF EXISTS denom_commerciale;
DROP TABLE IF EXISTS ref_marque;

CREATE TABLE denom_commerciale (Equiv VARCHAR(64) NULL, nom_commercial VARCHAR(128) NULL, nb int NULL,
nom_commercial_1 VARCHAR(128) NULL, nom_commercial_2 VARCHAR(128) NULL);

CREATE TABLE ref_marque (marque VARCHAR(64) NULL, Equiv VARCHAR(128) NULL);

LOAD DATA INFILE '/input_data/Clean_commercial_names.csv' into table denom_commerciale fields terminated by ';'
     optionally enclosed by '"'ignore 1 LINES (@dummy, Equiv, nom_commercial, nb,@dummy, nom_commercial_1, nom_commercial_2) ;

LOAD DATA INFILE '/input_data/ref_marque_new.csv' into table ref_marque fields terminated by ';'
     optionally enclosed by '"'ignore 1 LINES (marque, Equiv) ;

CREATE INDEX idx_ref_marque_marque ON ref_marque (marque);
CREATE INDEX idx_denom_com_nom_com ON denom_commerciale (nom_commercial);
CREATE INDEX idx_denom_com_nom_com_2 ON denom_commerciale (nom_commercial_2);
CREATE INDEX idx_denom_com_Equiv ON denom_commerciale (Equiv);