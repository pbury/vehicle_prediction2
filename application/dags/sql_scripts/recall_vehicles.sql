CREATE TABLE VEHC_FINAL as
SELECT tt.*, pv.ref_vehc_id
FROM table_test tt
JOIN (equiv_dos_id_test edit JOIN predicted_vehicles pv on pv.ref_vehc_id = edit.ref_vehc_id )
    on tt.dos_id = edit.dos_id;

