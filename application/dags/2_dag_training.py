from airflow import DAG
from airflow.operators.python import PythonOperator
from datetime import datetime
from scripts import Shaping_data_script_train, NeuralNetwork_scripts

dag = DAG(dag_id="2_dag_training",
          start_date=datetime(2021, 1, 1),
          schedule_interval="@monthly",
          catchup=False)

python_shaping_train = PythonOperator(
    task_id='shaping_train',
    python_callable=Shaping_data_script_train.main,
    dag=dag
)

python_train = PythonOperator(
    task_id='training',
    python_callable=NeuralNetwork_scripts.main,
    dag=dag
)

python_shaping_train >> python_train
