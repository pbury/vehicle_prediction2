from airflow import DAG
from airflow.operators.python import PythonOperator
from datetime import datetime
from airflow.operators.mysql_operator import MySqlOperator
from scripts import DensiteVehcbyDept_script
import os

dag = DAG(dag_id="1_dag_database_creation",
          start_date=datetime(2021, 1, 1),
          schedule_interval="@monthly",
          catchup=False)

create_db = MySqlOperator(dag=dag,
                          mysql_conn_id='mysql',
                          task_id='create_db',
                          sql='sql_scripts/create_db.sql')

loading_raw_file = MySqlOperator(dag=dag,
                                 mysql_conn_id='histovec',
                                 task_id='Import_de_la_base_brute',
                                 sql='sql_scripts/loading_raw_file.sql')

uniformisation_script = MySqlOperator(dag=dag,
                                      mysql_conn_id='histovec',
                                      task_id='Uniformisation_base_et_encodage_variables_categorielles',
                                      sql='sql_scripts/Uniformisation_script.sql')

dedoublonnage_vehicule = MySqlOperator(dag=dag,
                                       mysql_conn_id='histovec',
                                       task_id='Dedoublonnage_base_vehicule',
                                       sql='sql_scripts/dedoublonnage_vehicule.sql')

ref_carrosserie = MySqlOperator(dag=dag,
                                mysql_conn_id='histovec',
                                task_id='Creation_reference_carrosserie',
                                sql='sql_scripts/ref_carrosserie.sql')

ref_nom_com = MySqlOperator(dag=dag,
                            mysql_conn_id='histovec',
                            task_id='Creation_reference_noms_commerciaux',
                            sql='sql_scripts/ref_nom_com.sql')

densiteVehcbyDept = PythonOperator(task_id='VehcDept',
                                   python_callable=DensiteVehcbyDept_script.main,
                                   dag=dag
                                   )

normalization_continuous_variable = MySqlOperator(dag=dag,
                                                  mysql_conn_id='histovec',
                                                  task_id='Travail_sur_variables_numeriques',
                                                  sql='sql_scripts/normalization_continuous_variable.sql')

cleaning_logs = MySqlOperator(dag=dag,
                              mysql_conn_id='histovec',
                              task_id='Nettoyage_logs_et_separation_train_et_test',
                              sql='sql_scripts/cleaning_logs.sql')

prepare_train = MySqlOperator(dag=dag,
                              mysql_conn_id='histovec',
                              task_id='Creation_base_entrainement',
                              sql='sql_scripts/prepare_train.sql')

prepare_predict = MySqlOperator(dag=dag,
                                mysql_conn_id='histovec',
                                task_id='Creation_base_prediction',
                                sql='sql_scripts/prepare_predict.sql')

Dedoublonnage_base_train = MySqlOperator(dag=dag,
                                         mysql_conn_id='histovec',
                                         task_id='Dedoublonnage_base_entrainement_finale',
                                         sql='sql_scripts/Dedoublonnage_base_train.sql')

Dedoublonnage_base_predict = MySqlOperator(dag=dag,
                                           mysql_conn_id='histovec',
                                           task_id='Dedoublonnage_base_prediction_finale',
                                           sql='sql_scripts/Dedoublonnage_base_predict.sql')

create_db >> loading_raw_file >> uniformisation_script >> dedoublonnage_vehicule >> [ref_carrosserie, ref_nom_com] >> \
densiteVehcbyDept >> normalization_continuous_variable >> cleaning_logs >> prepare_train >> prepare_predict >> \
Dedoublonnage_base_train >> Dedoublonnage_base_predict
