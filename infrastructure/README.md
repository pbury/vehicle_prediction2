# Tester Postgresl + Madlib

Il faut installer un client postgres :

```apt-get install pgcli ```

puis le lancer

```
pgcli -h localhost -U postgres -d postgres
```

Lors de la création du container docker, nous avons injecté des données dans la base:


| Serial No. | GRE Score | TOEFL Score | University Rating | SOP | LOR | CGPA | Research | Chance of Admit   |
| ---------- | --------- | ----------- | ----------------- | --- | --- | ---- | -------- |-------------------|
| 1 | 337 | 118 | 4 | 4.5 | 4.5 | 9.65 | 1 | 0.92              |
| 2 | 324 | 107 | 4 | 4 | 4.5 | 8.87 | 1 | 0.76|



Pour tester la régression logistique, il faut convertir la dernière colonne de la table des admissions en entier.

```postgresql
ALTER TABLE admission ALTER COLUMN chance_of_admit TYPE integer;
```


puis entrainer le modèle
```postgresql
SELECT logregr_train( 'admission',                                                                       -- Source table
                      'admission_logregr',                                                               -- Output table
                      'chance_of_admit',                                                                 -- Dependent variable
                      'ARRAY[1, gre_score, toefl_score, university_rating, sop, lor, cgpa, research]',   -- Feature vector
                      NULL,                                                                              -- Grouping
                      20,                                                                                -- Max iterations
                      'irls'                                                                             -- Optimizer to use
                    );
```

Enfin, on peut voir les prédictions :
```postgresql
SELECT a.serial_no, logregr_predict(coef, ARRAY[1, gre_score, toefl_score, university_rating, sop, lor, cgpa, research]),
       a.chance_of_admit::BOOLEAN
FROM admission a, admission_logregr m
ORDER BY a.serial_no;
```

Voici un petit extrait des résultats


| serial_no   | logregr_predict   | chance_of_admit    |
| ---------- | ------------------ |--------------------|
| 1           | True              | True               |
| 2           | True              | True               |
| 3           | True              | True               |
| 4           | True              | True               |
| 5           | True              | True               |


Pour mémoire la doc est ici :  [Exemples MaDlib](https://madlib.apache.org/docs/latest/group__grp__logreg.html#examples)
